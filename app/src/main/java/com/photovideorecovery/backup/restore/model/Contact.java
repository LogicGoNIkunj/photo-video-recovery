package com.photovideorecovery.backup.restore.model;

public class Contact {

    public String contactRawId;
    public String contactId;
    public String contactDeleted;
    public String contactName;

    public Contact(String contactRawId, String contactId, String contactName, String contactDeleted) {
        this.contactRawId = contactRawId;
        this.contactId = contactId;
        this.contactName = contactName;
        this.contactDeleted = contactDeleted;
    }

    public String getContactRawId() {
        return this.contactRawId;
    }

    public String getContactContactId() {
        return this.contactId;
    }

    public String getContactDeleted() {
        return this.contactDeleted;
    }

    public String getContactName() {
        return this.contactName;
    }

}
