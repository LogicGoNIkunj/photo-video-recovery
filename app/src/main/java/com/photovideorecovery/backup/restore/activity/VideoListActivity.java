package com.photovideorecovery.backup.restore.activity;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.ads.nativead.NativeAdView;
import com.photovideorecovery.backup.restore.R;
import com.photovideorecovery.backup.restore.common.VideoScanner;
import com.photovideorecovery.backup.restore.utils.RecoverApp;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public class VideoListActivity extends AppCompatActivity {

    public static ArrayList<String> folderList = new ArrayList<>();
    public static ArrayList<ArrayList<String>> imagePathList = new ArrayList<>();
    private ArrayList<String> deletedImagesList = new ArrayList<>();
    private ArrayList<String> thumbnailList = new ArrayList<>();

    public VideoAdapter videoAdapter;
    public String selectedFolder;

    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            deletedImagesList = intent.getStringArrayListExtra("Deleted List");
            selectedFolder = intent.getStringExtra("folder_name");
        }
    };

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_video_list);
        admobBigNative(findViewById(R.id.flNativeAdd));
        initView();
    }

    @SuppressLint("MissingPermission")
    public void admobBigNative(FrameLayout admob_native_ll) {
        AdLoader.Builder builder = new AdLoader.Builder(this, RecoverApp.get_Admob_native_Id()).forNativeAd(nativeAd -> {
            @SuppressLint("InflateParams") NativeAdView adView = (NativeAdView) getLayoutInflater().inflate(R.layout.native_ads_small, null);
            populateUnifiedNativeAdView(nativeAd, adView);
            admob_native_ll.removeAllViews();
            admob_native_ll.addView(adView);
        });
        AdLoader adLoader = builder.withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);

            }
        }).build();
        adLoader.loadAd(new AdRequest.Builder().build());
    }

    public void populateUnifiedNativeAdView(NativeAd nativeAd, NativeAdView adView) {
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_app_icon));
        ((TextView) Objects.requireNonNull(adView.getHeadlineView())).setText(nativeAd.getHeadline());
        if (nativeAd.getBody() == null) {
            Objects.requireNonNull(adView.getBodyView()).setVisibility(View.INVISIBLE);
        } else {
            Objects.requireNonNull(adView.getBodyView()).setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        }
        if (nativeAd.getCallToAction() == null) {
            Objects.requireNonNull(adView.getCallToActionView()).setVisibility(View.INVISIBLE);
        } else {
            Objects.requireNonNull(adView.getCallToActionView()).setVisibility(View.VISIBLE);
            ((TextView) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
        }
        if (nativeAd.getIcon() == null) {
            Objects.requireNonNull(adView.getIconView()).setVisibility(View.GONE);
        } else {
            ((ImageView) Objects.requireNonNull(adView.getIconView())).setImageDrawable(nativeAd.getIcon().getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }
        adView.setNativeAd(nativeAd);
    }

    private void initView() {
        ((Toolbar) findViewById(R.id.toolbar)).setNavigationIcon(R.drawable.ic_back);
        ((Toolbar) findViewById(R.id.toolbar)).setNavigationOnClickListener(v -> onBackPressed());
        ((TextView) findViewById(R.id.tvTitle)).setText(R.string.str_deleted_video_recovery);

        findViewById(R.id.ivMenu).setVisibility(View.GONE);
    }

    @Override
    public void onDestroy() {
        if (broadcastReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(this.broadcastReceiver);
        }
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        new Handler().postDelayed(this::loadImage, 50);
    }

    private void loadImage() {
        LocalBroadcastManager.getInstance(this).registerReceiver(this.broadcastReceiver, new IntentFilter("Deleted Paths"));
        if (deletedImagesList == null) {
            deletedImagesList = new ArrayList<>();
        }
        HashMap<String, ArrayList<String>> folderOfVideo = VideoScanner.folderImage;
        Iterator<String> it = this.deletedImagesList.iterator();
        while (it.hasNext()) {
            try {
                Objects.requireNonNull(folderOfVideo.get(this.selectedFolder)).remove(it.next());
            } catch (Exception unused) {
                unused.printStackTrace();
            }
        }
        if (folderList != null) {
            folderList.clear();
            imagePathList.clear();
            thumbnailList.clear();
        } else {
            folderList = new ArrayList<>();
            imagePathList = new ArrayList<>();
            thumbnailList = new ArrayList<>();
        }
        for (String str : folderOfVideo.keySet()) {
            if (Objects.requireNonNull(folderOfVideo.get(str)).size() > 0) {
                imagePathList.add(folderOfVideo.get(str));
            }
        }
        Collections.sort(imagePathList, (arrayList, arrayList2) -> arrayList2.size() - arrayList.size());
        for (ArrayList<String> next : imagePathList) {
            try {
                for (String deletedImage : this.deletedImagesList) {
                    next.remove(deletedImage);
                }
                if (next.size() > 0) {
                    String str2 = next.get(0);

                    this.thumbnailList.add(str2);
                    String[] split = str2.split("/");
                    folderList.add(str2.substring(str2.length() - split[split.length - 1].length()));
                }
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
        videoAdapter = new VideoAdapter(this, this.thumbnailList);
        new Handler().postDelayed(() -> {
            findViewById(R.id.progressBar).setVisibility(View.GONE);
            ((RecyclerView) findViewById(R.id.recyclerview)).setAdapter(videoAdapter);
        }, 500);
    }

    @Override
    public void onBackPressed() {
        if (folderList != null)
            folderList = null;
        if (imagePathList != null)
            imagePathList = null;
        if (deletedImagesList != null)
            deletedImagesList = null;
        if (thumbnailList != null)
            thumbnailList = null;
        if (videoAdapter != null)
            videoAdapter = null;
        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
        finish();
    }

    public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.ViewHolder> {
        public int background;
        public final TypedValue typedValue = new TypedValue();
        public List<String> thumbnailList;

        public class ViewHolder extends RecyclerView.ViewHolder {
            public final TextView tvCount;
            public final ImageView ivMain;

            public ViewHolder(View view) {
                super(view);
                this.ivMain = view.findViewById(R.id.image);
                this.tvCount = view.findViewById(R.id.tvFolderCount);
            }

        }

        public VideoAdapter(Context context, List<String> list) {
            context.getTheme().resolveAttribute(R.attr.selectableItemBackground, this.typedValue, true);
            this.background = this.typedValue.resourceId;
            this.thumbnailList = list;
        }

        @Override
        public int getItemCount() {
            return this.thumbnailList.size();
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_image, viewGroup, false);
            inflate.setBackgroundResource(this.background);
            return new ViewHolder(inflate);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {

            ArrayList<String> imageList = imagePathList.get(position);
            if (imageList.size() != 0) {

                viewHolder.tvCount.setText(Objects.requireNonNull(new File(imageList.get(0)).getParentFile()).getName());

                Glide.with(viewHolder.ivMain.getContext())
                        .asBitmap()
                        .load(this.thumbnailList.get(position))
                        .placeholder(R.drawable.ic_loading_image)
                        .fitCenter()
                        .into(viewHolder.ivMain);

                viewHolder.ivMain.setOnClickListener(view -> {
                    try {
                        Intent intent = new Intent(VideoListActivity.this, VideoFolderActivity.class);
                        if (imageList.size() != 0)
                            intent.putExtra("position", position);
                        else
                            intent.putExtra("position", position + 1);

                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                    } catch (Exception unused) {
                        Toast.makeText(VideoListActivity.this, getString(R.string.str_something_went_wrong), Toast.LENGTH_SHORT).show();
                    }
                });
            }

        }
    }
}
