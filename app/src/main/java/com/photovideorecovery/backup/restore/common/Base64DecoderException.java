package com.photovideorecovery.backup.restore.common;

public class Base64DecoderException extends Exception {
    public Base64DecoderException(String str) {
        super(str);
    }
}
