package com.photovideorecovery.backup.restore.extension

import android.app.Activity
import android.content.Context
import android.content.Intent

inline fun <reified T> Context.startActivity(block:Intent.() -> Unit) {
    val intent = Intent(this, T::class.java)
    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
    intent.block()
    startActivity(intent)
}

inline fun <reified T> Context.startActivityForResult(requestCode: Int, block: Intent.() -> Unit) {
    val intent = Intent(this, T::class.java)
    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
    intent.block()
    (this as Activity).startActivityForResult(intent, requestCode)
}