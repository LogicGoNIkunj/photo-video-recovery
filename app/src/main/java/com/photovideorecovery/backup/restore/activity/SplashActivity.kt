package com.photovideorecovery.backup.restore.activity

import android.os.Bundle
import android.os.Handler
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.graphics.BlendModeColorFilterCompat
import androidx.core.graphics.BlendModeCompat
import com.photovideorecovery.backup.restore.R
import com.photovideorecovery.backup.restore.api_manager.API
import com.photovideorecovery.backup.restore.api_manager.Model_Ads
import com.photovideorecovery.backup.restore.extension.getCompactColor
import com.photovideorecovery.backup.restore.utils.RecoverApp
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.Boolean

class SplashActivity : AppCompatActivity() {


    companion object {
        var publisher_yes = false
    }

    public override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)
        runOnUiThread { setUpAnimation() }
        getAdsData()
        Handler().postDelayed(
            { RecoverApp.mAppOpenManager?.showAdIfAvailable(true) },
            5000
        )
    }

    private fun setUpAnimation() {

        val progressBar = ProgressBar(this).apply {
            indeterminateDrawable.colorFilter =
                BlendModeColorFilterCompat.createBlendModeColorFilterCompat(
                    getCompactColor(R.color.colorBlue),
                    BlendModeCompat.SRC_ATOP
                )
        }
        val layoutParams = RelativeLayout.LayoutParams(100, 100).apply {
            addRule(14)
            addRule(12)
            setMargins(0, 0, 0, 50)
        }
        val relativeLayout = RelativeLayout(this).apply {
            addView(progressBar, layoutParams)
        }

        setContentView(relativeLayout)
    }

    private fun getAdsData() {
        val retrofit: Retrofit.Builder = Retrofit.Builder().baseUrl(API.adsAPI)
            .addConverterFactory(GsonConverterFactory.create())
        val service: API = retrofit.build().create(API::class.java)
        val call: Call<Model_Ads> = service.getAdsData(44)
        call.enqueue(object : Callback<Model_Ads> {
            override fun onResponse(call: Call<Model_Ads>, response: Response<Model_Ads>) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body()!!.isStatus) {
                            if (response.body()!!.data != null) {
                                RecoverApp.set_AdsInt(response.body()!!.data.publisher_id)
                                if (response.body()!!.data.publishers != null) {
                                    publisher_yes = true
                                    if (RecoverApp.get_AdsInt() == 1) {
                                        if (response.body()!!.data.publishers.admob != null) {
                                            if (response.body()!!.data.publishers.admob.admob_interstitial != null) {
                                                RecoverApp.set_Admob_interstitial_Id(response.body()!!.data.publishers.admob.admob_interstitial.admob_interstitial_id)
                                            } else {
                                                RecoverApp.set_Admob_interstitial_Id(
                                                    resources.getString(
                                                        R.string.admob_Interstitial_ad
                                                    )
                                                )
                                            }
                                            if (response.body()!!.data.publishers.admob.admob_native != null) {
                                                RecoverApp.set_Admob_native_Id(response.body()!!.data.publishers.admob.admob_native.admob_Native_id)
                                            } else {
                                                RecoverApp.set_Admob_native_Id(resources.getString(R.string.admob_native_ad_key))
                                            }

                                            if (response.body()!!.data.publishers.admob.admob_open != null) {
                                                RecoverApp.set_Admob_openapp(response.body()!!.data.publishers.admob.admob_open.admob_Open_id)
                                            } else {
                                                RecoverApp.set_Admob_openapp(resources.getString(R.string.open_app_ad_id))
                                            }

                                            if (response.body()!!.data.publishers.admob.qureka_open != null) {
                                                RecoverApp.set_qureka(Boolean.parseBoolean(response.body()!!.data.publishers.admob.qureka_open.querka_id))
                                            } else {
                                                RecoverApp.set_qureka(false)
                                            }
                                        } else {
                                            RecoverApp.set_Admob_interstitial_Id(
                                                resources.getString(
                                                    R.string.admob_Interstitial_ad
                                                )
                                            )
                                            RecoverApp.set_Admob_native_Id(resources.getString(R.string.admob_native_ad_key))
                                            RecoverApp.set_Admob_openapp(resources.getString(R.string.open_app_ad_id))
                                            RecoverApp.set_qureka(false)
                                        }
                                    } else {
                                        RecoverApp.set_Admob_interstitial_Id(resources.getString(R.string.admob_Interstitial_ad))
                                        RecoverApp.set_Admob_native_Id(resources.getString(R.string.admob_native_ad_key))
                                        RecoverApp.set_Admob_openapp(resources.getString(R.string.open_app_ad_id))
                                        RecoverApp.set_qureka(false)
                                    }
                                } else {
                                    RecoverApp.set_Admob_interstitial_Id("1234")
                                    RecoverApp.set_Admob_native_Id("1234")
                                    RecoverApp.set_Admob_openapp("1234")
                                    RecoverApp.set_qureka(false)
                                }
                            } else {
                                RecoverApp.set_Admob_interstitial_Id(resources.getString(R.string.admob_Interstitial_ad))
                                RecoverApp.set_Admob_native_Id(resources.getString(R.string.admob_native_ad_key))
                                RecoverApp.set_Admob_openapp(resources.getString(R.string.open_app_ad_id))
                                RecoverApp.set_qureka(false)
                            }
                        } else {
                            RecoverApp.set_Admob_interstitial_Id(resources.getString(R.string.admob_Interstitial_ad))
                            RecoverApp.set_Admob_native_Id(resources.getString(R.string.admob_native_ad_key))
                            RecoverApp.set_Admob_openapp(resources.getString(R.string.open_app_ad_id))
                            RecoverApp.set_qureka(false)
                        }
                    } else {
                        RecoverApp.set_Admob_interstitial_Id(resources.getString(R.string.admob_Interstitial_ad))
                        RecoverApp.set_Admob_native_Id(resources.getString(R.string.admob_native_ad_key))
                        RecoverApp.set_Admob_openapp(resources.getString(R.string.open_app_ad_id))
                        RecoverApp.set_qureka(false)
                    }
                } else {
                    RecoverApp.set_Admob_interstitial_Id(resources.getString(R.string.admob_Interstitial_ad))
                    RecoverApp.set_Admob_native_Id(resources.getString(R.string.admob_native_ad_key))
                    RecoverApp.set_Admob_openapp(resources.getString(R.string.open_app_ad_id))
                    RecoverApp.set_qureka(false)
                }
            }

            override fun onFailure(call: Call<Model_Ads>, t: Throwable) {
                RecoverApp.set_Admob_interstitial_Id(resources.getString(R.string.admob_Interstitial_ad))
                RecoverApp.set_Admob_native_Id(resources.getString(R.string.admob_native_ad_key))
                RecoverApp.set_Admob_openapp(resources.getString(R.string.open_app_ad_id))
                RecoverApp.set_qureka(false)
            }

        })
    }

}