package com.photovideorecovery.backup.restore.activity

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.photovideorecovery.backup.restore.BuildConfig
import com.photovideorecovery.backup.restore.R
import com.photovideorecovery.backup.restore.common.Utils
import com.photovideorecovery.backup.restore.extension.*
import com.photovideorecovery.backup.restore.utils.Constant
import com.photovideorecovery.backup.restore.utils.RecoverApp


class StartActivity : AppCompatActivity(R.layout.activity_start) {

    private var doubleBackToExitPressedOnce = false


    override fun onStart() {
        super.onStart()
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = getCompactColor(R.color.blue_50)
    }

    fun onClick(view: View) {
        if (view.id == R.id.btnGetStarted) {
            setBoolean(Constant.IS_PERMISSION_SCREEN_SHOWED, true)
            if (Utils.hasPermissions(this, *Constant.PERMISSION)) {
                if (!checkManageStoragePermission()) {
                    requestPermission()
                } else
                    startActivity<HomeActivity> { }
            } else
                ActivityCompat.requestPermissions(
                    this,
                    Constant.PERMISSION,
                    Constant.PERMISSION_CODE
                )
        }
    }

    private fun requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            try {
                val intent = Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION)
                intent.addCategory(Intent.CATEGORY_DEFAULT)
                intent.data = Uri.parse(String.format("package:%s", packageName))
                startActivityForResult(intent, Constant.MANAGE_PERMISSION_CODE)
            } catch (e: Exception) {
                val intent = Intent()
                intent.action = Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION
                startActivityForResult(intent, Constant.MANAGE_PERMISSION_CODE)
            }
        } else {
            ActivityCompat.requestPermissions(
                this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                Constant.PERMISSION_CODE
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constant.MANAGE_PERMISSION_CODE) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                if (!Environment.isExternalStorageManager()) {
                    Toast.makeText(this, "Allow permission for storage access!", Toast.LENGTH_SHORT)
                        .show()
                    return
                }
                RecoverApp.mAppOpenManager!!.isAdShow = true
                startActivity<HomeActivity> { }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == Constant.PERMISSION_CODE) {
            if (!Utils.hasPermissions(this, *Constant.PERMISSION)) {
                if (!shouldShowRequestPermissionRationale(Constant.PERMISSION)) {
                    showSettingDialog()
                    return
                }
                return
            }
            if (!checkManageStoragePermission()) {
                requestPermission()
                return
            }
            startActivity<HomeActivity> { }
        }
    }

    private fun showSettingDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Need Permissions")
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.")
        builder.setPositiveButton("GO SETTINGS") { dialogInterface: DialogInterface, _: Int ->
            dialogInterface.cancel()
            openSettings()
        }
        builder.setNegativeButton("Cancel") { dialogInterface: DialogInterface, _: Int -> dialogInterface.cancel() }
        builder.show()
    }

    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        intent.data = Uri.fromParts("package", BuildConfig.APPLICATION_ID, null)
        startActivityForResult(intent, 101)
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            RecoverApp.mAppOpenManager.isAdShow = true
            finishAffinity()
            return
        }

        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show()

        Handler(Looper.getMainLooper()).postDelayed({
            doubleBackToExitPressedOnce = false
        }, 2000)
    }
}