package com.photovideorecovery.backup.restore.common;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.photovideorecovery.backup.restore.R;
import com.photovideorecovery.backup.restore.activity.ScanVideoActivity;
import com.photovideorecovery.backup.restore.activity.VideoListActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Stack;

@SuppressLint("StaticFieldLeak")
public class VideoScanner extends AsyncTask<Void, HashMap<String, ArrayList<String>>, Void> {

    private static final long HOVER_HIDE_TIMEOUT_SHORT_MS = 500;
    public static HashMap<String, ArrayList<String>> folderImage = new HashMap<>();

    public int fileCount;
    //    public int totalFileCount;
    public ScanVideoActivity activity;
    private long fileSize = 0;
    private final TextView tvPer;
    /*private final RoundedHorizontalProgressBar progressBar;*/
    private final LottieAnimationView animationView;

    public VideoScanner(ScanVideoActivity activity,
                        TextView tvPer,
                        LottieAnimationView animationView) {
        this.activity = activity;
        this.tvPer = tvPer;
        this.animationView = animationView;
        this.fileCount = 0;
//        this.totalFileCount = 0;

    }

    public Void videoScan() {


        File[] listFiles;
        Stack<String> stack = new Stack<>();
        Stack<String> tempStack = new Stack<>();
        HashSet<String> externalMounts = Utils.getExternalMounts();
        if (externalMounts.size() > 0) {
            for (String externalMount : externalMounts) {
                File[] listFiles2 = new File(externalMount).listFiles();
                if (listFiles2 != null && listFiles2.length >= 1) {
                    for (File file : listFiles2) {
                        stack.push(file.getAbsolutePath());
                        tempStack.push(file.getAbsolutePath());
                    }
                }
            }
        }

        while (!stack.isEmpty()) {
            String str = stack.pop();
            File file = new File(str);
            if (!file.isFile() && (listFiles = file.listFiles()) != null && listFiles.length >= 1) {
                ArrayList<String> arrayList = new ArrayList<>();
                for (File file1 : listFiles) {
                    if (file1.isDirectory()) {
                        stack.push(file1.getAbsolutePath());
                    } else if (str.contains("/.") && Utils.isVideoFile(file1)) {
                        arrayList.add(file1.getAbsolutePath());
                        this.fileSize = file1.length() + this.fileSize;
                        this.fileCount++;
                    }
                }
                if (arrayList.size() > 0)
                    folderImage.put(str, arrayList);

                //noinspection unchecked
                publishProgress(folderImage);
            }
        }
        return null;
    }


    public void navigate() {
        if (!isCancelled()) {
            if (this.fileCount != 0) {
                Intent intent = new Intent(this.activity, VideoListActivity.class);
                intent.putExtra("size", this.fileCount);
                this.activity.startActivity(intent);
                new Handler().postDelayed(() -> {
                    System.runFinalization();
                    Runtime.getRuntime().gc();
                    System.gc();
                    activity.finish();
                }, 100);
                return;
            }
            this.tvPer.setText(this.activity.getResources().getString(R.string.no_image));
            animationView.setVisibility(View.GONE);
        } else {
            animationView.pauseAnimation();
        }
    }

    @Override
    public Void doInBackground(Void[] voidArr) {
        return videoScan();
    }

    @Override
    public void onPostExecute(Void r4) {
        super.onPostExecute(r4);
        if (fileCount == 0) {
            this.tvPer.setText(R.string.no_video);
            animationView.setVisibility(View.GONE);
            animationView.pauseAnimation();
            return;
        }

        new Handler().postDelayed(this::navigate, HOVER_HIDE_TIMEOUT_SHORT_MS);
    }

    public void onPreExecute() {
        super.onPreExecute();
        tvPer.setText(R.string.please_wait);
    }

    @Override
    public void onProgressUpdate(HashMap<String, ArrayList<String>>[] hashMapArr) {
        super.onProgressUpdate(hashMapArr);
        if (this.fileCount != 0) {
            tvPer.setText(String.format(Locale.getDefault(), "Detected video %d", fileCount));

        }

    }
}
