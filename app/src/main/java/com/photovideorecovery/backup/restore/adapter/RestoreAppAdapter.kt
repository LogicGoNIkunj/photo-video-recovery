package com.photovideorecovery.backup.restore.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.photovideorecovery.backup.restore.R
import com.photovideorecovery.backup.restore.diffutils.RestoreAppDiffUtils
import com.photovideorecovery.backup.restore.model.RestoreModel

class RestoreAppAdapter : RecyclerView.Adapter<RestoreAppAdapter.Holder>() {

    var onItemClickListener: OnItemClickListener? = null

    var restoreAppList = arrayListOf<RestoreModel>()
        set(value) {
            restoreAppList.clear()
            restoreAppList.addAll(value)
            DiffUtil.calculateDiff(RestoreAppDiffUtils(this.restoreAppList, value)).dispatchUpdatesTo(this)
        }

    class Holder(item: View) : RecyclerView.ViewHolder(item) {
        var tvTitle: TextView = item.findViewById(R.id.name)
        var image: ImageView = item.findViewById(R.id.image)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_backup_app, parent, false)
        return Holder(item = view)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.tvTitle.text = this.restoreAppList[position].name
        holder.image.setImageDrawable(this.restoreAppList[position].icon)

        holder.itemView.setOnClickListener { onItemClickListener?.itemClick(restoreAppList[position]) }
    }

    override fun getItemCount(): Int {
        return restoreAppList.size
    }

    interface OnItemClickListener {
        fun itemClick(backupModel: RestoreModel)
    }
}