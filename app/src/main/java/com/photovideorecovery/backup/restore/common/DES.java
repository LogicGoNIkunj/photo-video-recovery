package com.photovideorecovery.backup.restore.common;

public class DES {
    public static int[] IP = {58, 50, 42, 34, 26, 18, 10, 2, 60, 52, 44, 36, 28, 20, 12, 4, 62, 54, 46, 38, 30, 22, 14, 6, 64, 56, 48, 40, 32, 24, 16, 8, 57, 49, 41, 33, 25, 17, 9, 1, 59, 51, 43, 35, 27, 19, 11, 3, 61, 53, 45, 37, 29, 21, 13, 5, 63, 55, 47, 39, 31, 23, 15, 7};

    public static int[] PC1 = {57, 49, 41, 33, 25, 17, 9, 1, 58, 50, 42, 34, 26, 18, 10, 2, 59, 51, 43, 35, 27, 19, 11, 3, 60, 52, 44, 36, 63, 55, 47, 39, 31, 23, 15, 7, 62, 54, 46, 38, 30, 22, 14, 6, 61, 53, 45, 37, 29, 21, 13, 5, 28, 20, 12, 4};
    public static int[] PC2 = {14, 17, 11, 24, 1, 5, 3, 28, 15, 6, 21, 10, 23, 19, 12, 4, 26, 8, 16, 7, 27, 20, 13, 2, 41, 52, 31, 37, 47, 55, 30, 40, 51, 45, 33, 48, 44, 49, 39, 56, 34, 53, 46, 42, 50, 36, 29, 32};
    public static int[] expandTbl = {32, 1, 2, 3, 4, 5, 4, 5, 6, 7, 8, 9, 8, 9, 10, 11, 12, 13, 12, 13, 14, 15, 16, 17, 16, 17, 18, 19, 20, 21, 20, 21, 22, 23, 24, 25, 24, 25, 26, 27, 28, 29, 28, 29, 30, 31, 32, 1};
    public static byte[][] subKeys;
    public static int[] key = {16, 7, 20, 21, 29, 12, 28, 17, 1, 15, 23, 26, 5, 18, 31, 10, 2, 8, 24, 14, 32, 27, 3, 9, 19, 13, 30, 6, 22, 11, 4, 25};
    public static int[] invIP = {40, 8, 48, 16, 56, 24, 64, 32, 39, 7, 47, 15, 55, 23, 63, 31, 38, 6, 46, 14, 54, 22, 62, 30, 37, 5, 45, 13, 53, 21, 61, 29, 36, 4, 44, 12, 52, 20, 60, 28, 35, 3, 43, 11, 51, 19, 59, 27, 34, 2, 42, 10, 50, 18, 58, 26, 33, 1, 41, 9, 49, 17, 57, 25};
    public static int[] keyShift = {1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1};
    public static final int[][][] r0;
    public static int[][][] boxes;

    static {
        int[][][] iArr = new int[8][][];
        r0 = iArr;
        iArr[0] = new int[][]{new int[]{14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7}, new int[]{0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8}, new int[]{4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0}, new int[]{15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13}};
        iArr[1] = new int[][]{new int[]{15, 1, 8, 14, 6, 11, 3, 2, 9, 7, 2, 13, 12, 0, 5, 10}, new int[]{3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5}, new int[]{0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15}, new int[]{13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9}};
        iArr[2] = new int[][]{new int[]{10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8}, new int[]{13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1}, new int[]{13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7}, new int[]{1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12}};
        iArr[3] = new int[][]{new int[]{7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15}, new int[]{13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9}, new int[]{10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4}, new int[]{3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14}};
        iArr[4] = new int[][]{new int[]{2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9}, new int[]{14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6}, new int[]{4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14}, new int[]{11, 8, 12, 7, 1, 14, 2, 12, 6, 15, 0, 9, 10, 4, 5, 3}};
        iArr[5] = new int[][]{new int[]{12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11}, new int[]{10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8}, new int[]{9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6}, new int[]{4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13}};
        iArr[6] = new int[][]{new int[]{4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1}, new int[]{13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6}, new int[]{1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2}, new int[]{6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12}};
        iArr[7] = new int[][]{new int[]{13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7}, new int[]{1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2}, new int[]{7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8}, new int[]{2, 1, 14, 7, 4, 10, 18, 13, 15, 12, 9, 0, 3, 5, 6, 11}};
        boxes = iArr;
    }

    public static byte[] concatBits(byte[] bArr, int i, byte[] bArr2, int i2) {
        byte[] bArr3 = new byte[((((i + i2) - 1) / 8) + 1)];
        int i3 = 0;
        for (int i4 = 0; i4 < i; i4++) {
            setBit(bArr3, i3, extractBit(bArr, i4));
            i3++;
        }
        for (int i5 = 0; i5 < i2; i5++) {
            setBit(bArr3, i3, extractBit(bArr2, i5));
            i3++;
        }
        return bArr3;
    }

    public static byte[] decrypt(byte[] bArr, byte[] bArr2) {
        byte[] bArr3 = new byte[bArr.length];
        byte[] bArr4 = new byte[8];
        subKeys = generateSubKeys(bArr2);
        for (int i = 0; i < bArr.length; i++) {
            bArr[i] = (byte) (bArr[i] ^ 8);
        }
        int i2 = 0;
        while (i2 < bArr.length) {
            if (i2 > 0 && i2 % 8 == 0) {
                bArr4 = encrypt64Bloc(bArr4, subKeys, true);
                System.arraycopy(bArr4, 0, bArr3, i2 - 8, bArr4.length);
            }
            bArr4[i2 % 8] = bArr[i2];
            i2++;
        }
        byte[] encrypt64Bloc = encrypt64Bloc(bArr4, subKeys, true);
        System.arraycopy(encrypt64Bloc, 0, bArr3, i2 - 8, encrypt64Bloc.length);
        return deletePadding(bArr3);
    }

    public static byte[] deletePadding(byte[] bArr) {
        int i = 0;
        for (int length = bArr.length - 1; bArr[length] == 0; length--) {
            i++;
        }
        int length2 = (bArr.length - i) - 1;
        byte[] bArr2 = new byte[length2];
        System.arraycopy(bArr, 0, bArr2, 0, length2);
        return bArr2;
    }

    public static byte[] encrypt64Bloc(byte[] bArr, byte[][] bArr2, boolean z) {

        byte[] permuteFunc = permuteFunc(bArr, IP);
        int i = 0;
        byte[] extractBits;
        int[] iArr = IP;
        byte[] extractBits2 = extractBits(permuteFunc, iArr.length / 2, iArr.length / 2);
        while (true) {
            extractBits = extractBits2;
            if (i < 16) {
                extractBits2 = xor_func(extractBits, z ? f_func(extractBits, bArr2[15 - i]) : f_func(extractBits, bArr2[i]));
                i++;
            } else {
                int[] iArr2 = IP;
                return permuteFunc(concatBits(extractBits, iArr2.length / 2, extractBits, iArr2.length / 2), invIP);
            }
        }
    }

    public static int extractBit(byte[] bArr, int i) {
        return (bArr[i / 8] >> (8 - ((i % 8) + 1))) & 1;
    }

    public static byte[] extractBits(byte[] bArr, int i, int i2) {
        byte[] bArr2 = new byte[(((i2 - 1) / 8) + 1)];
        for (int i3 = 0; i3 < i2; i3++) {
            setBit(bArr2, i3, extractBit(bArr, i + i3));
        }
        return bArr2;
    }

    public static byte[] f_func(byte[] bArr, byte[] bArr2) {
        return permuteFunc(s_func(xor_func(permuteFunc(bArr, expandTbl), bArr2)), key);
    }

    public static byte[][] generateSubKeys(byte[] bArr) {
        byte[][] bArr2 = new byte[16][];
        byte[] permuteFunc = permuteFunc(bArr, PC1);
        byte[] extractBits = extractBits(permuteFunc, 0, PC1.length / 2);
        int[] iArr = PC1;
        byte[] extractBits2 = extractBits(permuteFunc, iArr.length / 2, iArr.length / 2);
        for (int i = 0; i < 16; i++) {
            extractBits = rotLeft(extractBits, 28, keyShift[i]);
            extractBits2 = rotLeft(extractBits2, 28, keyShift[i]);
            bArr2[i] = permuteFunc(concatBits(extractBits, 28, extractBits2, 28), PC2);
        }
        return bArr2;
    }

    public static byte[] permuteFunc(byte[] bArr, int[] iArr) {
        byte[] bArr2 = new byte[(((iArr.length - 1) / 8) + 1)];
        for (int i = 0; i < iArr.length; i++) {
            setBit(bArr2, i, extractBit(bArr, iArr[i] - 1));
        }
        return bArr2;
    }

    public static byte[] rotLeft(byte[] bArr, int i, int i2) {
        byte[] bArr2 = new byte[(((i - 1) / 8) + 1)];
        for (int i3 = 0; i3 < i; i3++) {
            setBit(bArr2, i3, extractBit(bArr, (i3 + i2) % i));
        }
        return bArr2;
    }

    public static byte[] s_func(byte[] bArr) {
        byte[] separateBytes = separateBytes(bArr, 6);
        byte[] bArr2 = new byte[(separateBytes.length / 2)];
        int i = 0;
        for (int i2 = 0; i2 < separateBytes.length; i2++) {
            byte b = separateBytes[i2];
            int i3 = boxes[i2][(((b >> 7) & 1) * 2) + ((b >> 2) & 1)][(b >> 3) & 15];
            if (i2 % 2 == 0) {
                i = i3;
            } else {
                bArr2[i2 / 2] = (byte) ((i * 16) + i3);
            }
        }
        return bArr2;
    }

    public static byte[] separateBytes(byte[] bArr, int i) {
        int length = (((bArr.length * 8) - 1) / i) + 1;
        byte[] bArr2 = new byte[length];
        for (int i2 = 0; i2 < length; i2++) {
            for (int i3 = 0; i3 < i; i3++) {
                setBit(bArr2, (i2 * 8) + i3, extractBit(bArr, (i * i2) + i3));
            }
        }
        return bArr2;
    }

    public static void setBit(byte[] bArr, int i, int i2) {
        int i3 = i / 8;
        int i4 = i % 8;
        bArr[i3] = (byte) (((byte) ((65407 >> i4) & bArr[i3] & 255)) | (i2 << (8 - (i4 + 1))));
    }

    public static byte[] xor_func(byte[] bArr, byte[] bArr2) {
        byte[] bArr3 = new byte[bArr.length];
        for (int i = 0; i < bArr.length; i++) {
            bArr3[i] = (byte) (bArr[i] ^ bArr2[i]);
        }
        return bArr3;
    }
}
