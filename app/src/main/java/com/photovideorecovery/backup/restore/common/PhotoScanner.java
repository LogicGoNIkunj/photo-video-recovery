package com.photovideorecovery.backup.restore.common;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.photovideorecovery.backup.restore.R;
import com.photovideorecovery.backup.restore.activity.PhotoListActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Stack;

@SuppressLint("StaticFieldLeak")
public class PhotoScanner extends AsyncTask<Void, HashMap<String, ArrayList<String>>, Void> {

    public static HashMap<String, ArrayList<String>> folderOfImage = new HashMap<>();
    private static final long HOVER_HIDE_TIMEOUT_SHORT_MS = 500;
    //    public int totalFileCount;
    public int fileCount;
    public long fileSize = 0;

    private final Activity activity;
    private final TextView tvPer;
    private final LottieAnimationView animationView;

    public PhotoScanner(Activity activity,
                        TextView tvPer, LottieAnimationView animationView) {

        this.activity = activity;
        this.tvPer = tvPer;
        this.animationView = animationView;
        this.fileCount = 0;
    }

    public void navigate() {
        if (!isCancelled()) {
            if (this.fileCount != 0) {
                Intent intent = new Intent(this.activity, PhotoListActivity.class);
                intent.putExtra("size", this.fileCount);
                this.activity.startActivity(intent);
                new Handler().postDelayed(() -> {
                    System.runFinalization();
                    Runtime.getRuntime().gc();
                    System.gc();
                    activity.finish();
                }, 100);
                return;
            }
            this.tvPer.setText(this.activity.getResources().getString(R.string.no_image));
            animationView.setVisibility(View.GONE);
        } else {
            animationView.pauseAnimation();
        }


    }

    public Void scanImage() {

        File[] listFiles;
//        Stack<String> tempStack = new Stack<>();
        Stack<String> stack = new Stack<>();
        HashSet<String> externalMounts = Utils.getExternalMounts();
        if (externalMounts.size() > 0) {
            for (String externalMount : externalMounts) {
                File[] listFiles2 = new File(externalMount).listFiles();
                if (listFiles2 != null && listFiles2.length >= 1) {
                    for (File file : listFiles2) {
                        stack.push(file.getAbsolutePath());
                    }
                }
            }

        }

        //add data into arraylist
        while (!stack.isEmpty()) {
            String str = stack.pop();
            File file2 = new File(str);
            if (!file2.isFile() && (listFiles = file2.listFiles()) != null && listFiles.length >= 1) {
                ArrayList<String> arrayList = new ArrayList<>();
                for (File file3 : listFiles) {
                    if (file3.isDirectory()) {
                        stack.push(file3.getAbsolutePath());
                    } else if (str.contains("/.") && Utils.isImageFile(file3)) {
                        arrayList.add(file3.getAbsolutePath());
                        fileSize += file3.length();
                        this.fileCount++;
                    }
                }

                if (arrayList.size() > 0)
                    folderOfImage.put(str, arrayList);

                if (fileCount != 0) {
                    //noinspection unchecked
                    publishProgress(folderOfImage);
                }
            }

        }
        return null;
    }

    @Override
    public Void doInBackground(Void[] voidArr) {
        return scanImage();
    }


    @Override
    public void onPostExecute(Void r4) {
        super.onPostExecute(r4);
        if (fileCount == 0) {
            this.tvPer.setText(R.string.no_image);
            animationView.setVisibility(View.GONE);
            animationView.pauseAnimation();
            return;
        }

        new Handler().postDelayed(this::navigate, HOVER_HIDE_TIMEOUT_SHORT_MS);

    }

    public void onPreExecute() {
        super.onPreExecute();
        tvPer.setText(activity.getString(R.string.please_wait));
    }

    @Override
    public void onProgressUpdate(HashMap<String, ArrayList<String>>[] hashMapArr) {
        super.onProgressUpdate(hashMapArr);
        if (this.fileCount != 0) {
            tvPer.setText(String.format(Locale.getDefault(), "Detected image %d", fileCount));

        }

    }


}
