package com.photovideorecovery.backup.restore.api_manager;

import com.photovideorecovery.backup.restore.model.FeedbackModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface API {

    String adsAPI = "https://stage-ads.punchapp.in/api/";
    String Qureka_API = "https://smartadz.in/api/";
    String Feedback_API = "https://punchapp.in/api/";


    @FormUrlEncoded
    @POST("feedback")
    Call<FeedbackModel> sendFeedback(
            @Field("app_name") String app_name,
            @Field("package_name") String package_name,
            @Field("title") String title,
            @Field("description") String description,
            @Field("device_name") String device_name,
            @Field("android_version") String android_version,
            @Field("version") String version
    );

    @GET("qureka-ad")
    Call<Model_Playdata> getBtnAd();

    @FormUrlEncoded
    @POST("get-ads-list")
    Call<Model_Ads> getAdsData(@Field("app_id") int app_id);
}
