package com.photovideorecovery.backup.restore.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.photovideorecovery.backup.restore.R
import com.photovideorecovery.backup.restore.diffutils.BackAppDiffUtils
import com.photovideorecovery.backup.restore.model.BackupModel

class BackAppAdapter : RecyclerView.Adapter<BackAppAdapter.Holder>() {

    var listener: OnItemClickListener? = null

    var appList = arrayListOf<BackupModel>()
        set(value) {
            appList.clear()
            appList.addAll(value)
            DiffUtil.calculateDiff(BackAppDiffUtils(this.appList, value)).dispatchUpdatesTo(this)
        }

    class Holder(item: View) : RecyclerView.ViewHolder(item) {
        var image: ImageView = item.findViewById(R.id.image)
        var tvTitle: TextView = item.findViewById(R.id.name)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_backup_app, parent, false)
        return Holder(view)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.tvTitle.text = appList[position].appName
        holder.image.setImageDrawable(appList[position].appIcon)

        holder.itemView.setOnClickListener {
            listener?.itemClick(appList[position])
        }
    }

    override fun getItemCount(): Int {
        return appList.size
    }

    interface OnItemClickListener {
        fun itemClick(backupModel: BackupModel)
    }
}