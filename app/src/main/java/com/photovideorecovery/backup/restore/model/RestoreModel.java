package com.photovideorecovery.backup.restore.model;

import android.graphics.drawable.Drawable;
import java.io.File;

public class RestoreModel {
    private long app_memory;
    private File file;
    private Drawable icon;
    private String name;
    private String path;


    public String getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String str) {
        this.path = str;
    }

    public File getFile() {
        return this.file;
    }

    public void setFile(File file2) {
        this.file = file2;
        setApp_memory(this.file.length());
    }

    public Drawable getIcon() {
        return this.icon;
    }

    public void setIcon(Drawable drawable) {
        this.icon = drawable;
    }

    public long getApp_memory() {
        return this.app_memory;
    }

    private void setApp_memory(long j) {
        this.app_memory = j;
    }
}
