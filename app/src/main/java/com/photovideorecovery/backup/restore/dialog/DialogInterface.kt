package com.photovideorecovery.backup.restore.dialog

interface DialogInterface {
    fun onPositiveClick()
    fun onNegativeClick()
}