package com.photovideorecovery.backup.restore.activity;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.nativead.MediaView;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.ads.nativead.NativeAdView;
import com.photovideorecovery.backup.restore.R;
import com.photovideorecovery.backup.restore.common.PhotoScanner;
import com.photovideorecovery.backup.restore.dialog.BottomSheetDialogFragment;
import com.photovideorecovery.backup.restore.dialog.DialogInterface;
import com.photovideorecovery.backup.restore.utils.RecoverApp;

import java.util.Objects;

public class ScanPhotoActivity extends AppCompatActivity {

    public static String PACKAGE;
    private PhotoScanner photoScanner;
    private LottieAnimationView animationView;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_scan_photo);
        new Handler().postDelayed(() -> loadBigNativeAd(findViewById(R.id.flNativeAdd)), 50);
        initView();
    }

    private void initView() {
        ((Toolbar) findViewById(R.id.toolbar)).setNavigationOnClickListener(v -> onBackPressed());

        animationView = findViewById(R.id.animationView);
        animationView.pauseAnimation();

        PACKAGE = getPackageName();

        photoScanner = new PhotoScanner(this,
                findViewById(R.id.tvPer),
                animationView
        );

    }

    public void loadBigNativeAd(FrameLayout admob_native_ll) {
        AdLoader.Builder builder = new AdLoader.Builder(this, RecoverApp.get_Admob_native_Id()).forNativeAd(nativeAd -> {
            NativeAdView adView = (NativeAdView) getLayoutInflater().inflate(R.layout.layout_unified_native_ad, null);
            populateUnifiedNativeAdView(nativeAd, adView);
            admob_native_ll.setVisibility(View.VISIBLE);
            admob_native_ll.removeAllViews();
            admob_native_ll.addView(adView);
        });
        AdLoader adLoader = builder.withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
            }
        }).build();
        adLoader.loadAd(new AdRequest.Builder().build());
    }

    public void populateUnifiedNativeAdView(NativeAd nativeAd, NativeAdView adView) {
        MediaView mediaView = adView.findViewById(R.id.ad_media);
        mediaView.setImageScaleType(ImageView.ScaleType.CENTER_CROP);
        adView.setMediaView(mediaView);
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_app_icon));
        ((TextView) Objects.requireNonNull(adView.getHeadlineView())).setText(nativeAd.getHeadline());
        if (nativeAd.getBody() == null) {
            Objects.requireNonNull(adView.getBodyView()).setVisibility(View.INVISIBLE);
        } else {
            Objects.requireNonNull(adView.getBodyView()).setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        }
        if (nativeAd.getCallToAction() == null) {
            Objects.requireNonNull(adView.getCallToActionView()).setVisibility(View.INVISIBLE);
        } else {
            Objects.requireNonNull(adView.getCallToActionView()).setVisibility(View.VISIBLE);
            ((TextView) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
        }
        if (nativeAd.getIcon() == null) {
            Objects.requireNonNull(adView.getIconView()).setVisibility(View.GONE);
        } else {
            ((ImageView) Objects.requireNonNull(adView.getIconView())).setImageDrawable(nativeAd.getIcon().getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }
        adView.setNativeAd(nativeAd);
    }


    public void onClick(View v) {
        if (v.getId() == R.id.btnShow) {
            findViewById(R.id.btnShow).setVisibility(View.GONE);
            animationView.setVisibility(View.VISIBLE);
            animationView.playAnimation();
            photoScanner.execute();
        }
    }

    @Override
    public void onBackPressed() {
        photoScanner.cancel(true);
        ((TextView) findViewById(R.id.tvPer)).setText("");
        if (animationView.isAnimating()) {
            animationView.pauseAnimation();
            showExitDialog();
        } else {
            System.runFinalization();
            Runtime.getRuntime().gc();
            System.gc();
            finish();
        }
    }

    private void showExitDialog() {
        new BottomSheetDialogFragment(
                "Exit",
                "Are you sure want to stop scan?",
                "Yes",
                "No",
                new DialogInterface() {
                    @Override
                    public void onPositiveClick() {
                        System.runFinalization();
                        Runtime.getRuntime().gc();
                        System.gc();
                        finish();
                    }

                    @Override
                    public void onNegativeClick() {
                        photoScanner = new PhotoScanner(
                                ScanPhotoActivity.this,
                                findViewById(R.id.tvPer),
                                animationView);

                        animationView.setVisibility(View.VISIBLE);
                        animationView.playAnimation();
                        photoScanner.execute();
                    }
                }
        ).show(getSupportFragmentManager(), "CustomBottomSheetDialogFragment");
    }

}
