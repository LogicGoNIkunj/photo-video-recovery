package com.photovideorecovery.backup.restore.extension

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Build.VERSION
import android.os.Environment
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

 fun Context.checkManageStoragePermission(): Boolean {
    return if (VERSION.SDK_INT >= Build.VERSION_CODES.R) {
        Environment.isExternalStorageManager()
    } else {
        val result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
        val result1 = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        result == PackageManager.PERMISSION_GRANTED || result1 == PackageManager.PERMISSION_GRANTED
    }
}


fun Context.hasPermissions(vararg permissions: String): Boolean {
    for (permission in permissions) {
        if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
            return false
        }
    }
    return true
}

fun Context.shouldShowRequestPermissionRationale(permission: Array<String>): Boolean {
    permission.forEach {
        if (!ActivityCompat.shouldShowRequestPermissionRationale(this as Activity, it)) {
            return false
        }
    }
    return true
}
