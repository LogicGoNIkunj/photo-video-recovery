package com.photovideorecovery.backup.restore.activity

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.photovideorecovery.backup.restore.R
import com.photovideorecovery.backup.restore.adapter.RecoverFileAdapter
import com.photovideorecovery.backup.restore.databinding.ActivityRecoveredBinding
import com.photovideorecovery.backup.restore.extension.*
import com.photovideorecovery.backup.restore.model.ImageObject
import com.photovideorecovery.backup.restore.utils.Constant
import com.photovideorecovery.backup.restore.utils.NpaGridLayoutManager
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File


class RecoveredActivity : AppCompatActivity() {

    private var selectedPage = 1

    private var imageList = listOf<ImageObject>()
    private var videoList = listOf<ImageObject>()

    private lateinit var binding: ActivityRecoveredBinding

    private val fileAdapter: RecoverFileAdapter by lazy { RecoverFileAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRecoveredBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.progressBar.show()
        initView()
        getPhotoList()
        getVideoList()

    }

    private fun initView() {
        binding.toolbar.setNavigationOnClickListener { finish() }
        binding.rvRecovered.apply {
            adapter = fileAdapter
            layoutManager = NpaGridLayoutManager(this@RecoveredActivity, 2)
        }

    }

    private fun getPhotoList() {
        CoroutineScope(Dispatchers.IO).launch {
            val list = File(
                Constant.BASE_PATH + getString(R.string.app_name) + File.separator + getString(R.string.photo) + File.separator
            ).listFiles()
            imageList = list?.map { ImageObject(it.absolutePath, false) } ?: listOf()
            withContext(Dispatchers.Main) {
                if (selectedPage == 1) showNoDtaFound(imageList.isEmpty())
                else showNoDtaFound(videoList.isEmpty())

                fileAdapter.list = imageList
                binding.progressBar.hide()
            }
        }
    }

    private fun getVideoList(isSetAdapter: Boolean = false) {
        CoroutineScope(Dispatchers.IO).launch {
            val list = File(
                Constant.BASE_PATH + getString(R.string.app_name) + File.separator + getString(R.string.video) + File.separator
            ).listFiles()
            videoList = list?.map { ImageObject(it.absolutePath, false) } ?: listOf()
            withContext(Dispatchers.Main) {
                binding.progressBar.hide()
                if (selectedPage == 1) showNoDtaFound(imageList.isEmpty())
                else showNoDtaFound(videoList.isEmpty())
                if (isSetAdapter) fileAdapter.list = videoList
            }
        }
    }

    private fun showNoDtaFound(isDataAvailable: Boolean) {
        if (isDataAvailable) {
            binding.ivNoDataFound.show()
            binding.rvRecovered.invisible()
            binding.groupSelectButton.hide()
        } else {
            binding.ivNoDataFound.hide()
            binding.groupSelectButton.show()
            binding.rvRecovered.show()
        }
    }

    fun onClick(view: View) {
        when (view.id) {
            R.id.btnSelectAll -> {
                fileAdapter.selectedList.addAll(fileAdapter.list)
                fileAdapter.list.forEachIndexed { index, _ ->
                    fileAdapter.list[index].isCheck = true
                }
                fileAdapter.notifyItemRangeChanged(0, fileAdapter.list.size)
            }
            R.id.btnUnSelectAll -> {
                fileAdapter.selectedList.clear()
                fileAdapter.list.forEachIndexed { index, _ ->
                    fileAdapter.list[index].isCheck = false
                }
                fileAdapter.notifyItemRangeChanged(0, fileAdapter.list.size)
            }
            R.id.tvPhotos -> {
                selectedPage = 1
                fileAdapter.selectedList.clear()
                fileAdapter.list = imageList
                showNoDtaFound(imageList.isEmpty())
                binding.tvPhotos.changeTint(R.color.red_400)
                binding.tvVideo.changeTint(android.R.color.transparent)
            }
            R.id.tvVideo -> {
                selectedPage = 2
                fileAdapter.selectedList.clear()
                fileAdapter.list = videoList
                showNoDtaFound(videoList.isEmpty())
                binding.tvVideo.changeTint(R.color.red_400)
                binding.tvPhotos.changeTint(android.R.color.transparent)
            }
            R.id.ivDelete -> {
                if (fileAdapter.selectedList.size == 0) {
                    showToast("Please select at least one file")
                    return
                }
                val dialog = AlertDialog.Builder(this)
                dialog.setMessage("Are you sure want to delete this file ?")
                dialog.setPositiveButton("Yes") { _dialog, _ ->
                    _dialog.dismiss()
                    deleteSelectedFile()
                }
                dialog.setNegativeButton("No") { _dialog, _ ->
                    _dialog.dismiss()
                }
                dialog.create().apply {
                    window?.setBackgroundDrawable(getCompactDrawable(R.drawable.back_white))
                }.show()
            }
        }
    }

    private fun deleteSelectedFile() {
        binding.progressBar.show()
        val list = fileAdapter.selectedList

        CoroutineScope(Dispatchers.IO).launch {
            var isFileDelete = true
            for (imageObject in list) {
                if (!File(imageObject.filePath).delete()) isFileDelete = false
            }
            withContext(Dispatchers.Main) {
                if (!isFileDelete)
                    showToast("Some file not delete please delete manually")

                if (list[0].filePath.endsWith(".png")||list[0].filePath.endsWith(".jpg")||list[0].filePath.endsWith(".jpeg")) {
                    if (list.size>1)
                       showToast("Images successfully deleted.")
                    else
                       showToast("Image successfully deleted.")

                    getPhotoList()
                    binding.tvPhotos.performClick()
                } else {
                    if (list.size>1)
                        showToast("Videos successfully deleted.")
                    else
                        showToast("Video successfully deleted.")

                    getVideoList(true)
                    binding.tvVideo.performClick()
                }
                binding.progressBar.hide()

            }
        }
    }
}