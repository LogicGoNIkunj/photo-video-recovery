package com.photovideorecovery.backup.restore.extension

import android.content.Context
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import com.photovideorecovery.backup.restore.utils.Constant

fun Context.setBoolean(key: String, value: Boolean) {
    val preferences = getSharedPreferences(Constant.PREFERENCE_MANAGER, Context.MODE_PRIVATE)
    val editor = preferences.edit()
    editor.putBoolean(key, value)
    editor.apply()
}

fun Context.getBoolean(key: String): Boolean {
    val preferences = getSharedPreferences(Constant.PREFERENCE_MANAGER, Context.MODE_PRIVATE)
    return preferences.getBoolean(key, false)
}

fun Context.getCompactColor(@ColorRes color: Int)  = ContextCompat.getColor(this, color)

fun Context.getCompactDrawable(@DrawableRes drawable:Int) = ContextCompat.getDrawable(this, drawable)