package com.photovideorecovery.backup.restore.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.Group;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.photovideorecovery.backup.restore.R;
import com.photovideorecovery.backup.restore.common.Utils;
import com.photovideorecovery.backup.restore.model.ImageObject;
import com.photovideorecovery.backup.restore.utils.Constant;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

@SuppressLint("NotifyDataSetChanged")
public class PhotoFolderActivity extends AppCompatActivity {

    public ImageAdapter imageAdapter;
    public ArrayList<ImageObject> imageList = new ArrayList<>();
    public ArrayList<String> folderList = new ArrayList<>();
    public ArrayList<String> savedSelectedPathList = new ArrayList<>();
    public ArrayList<String> selectedPathList = new ArrayList<>();
    public ArrayList<Integer> selectedPositionList = new ArrayList<>();
    public String folderName;
    public boolean isClicked = false;
    public int countCopied = 0;
    public int countError = 0;
    public int folderSize = 0;
    private ProgressBar progressBar;
    private Group groupBtn;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_photo_folder);

        findViews();
        initViews();
    }

    @SuppressLint("NonConstantResourceId")
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnRestore: {
                if (selectedPathList.size() == 0) {
                    Toast.makeText(this, "Please select at least one file", Toast.LENGTH_SHORT).show();
                    return;
                }
                new RestoreFileTask().execute();
                break;
            }
            case R.id.btnDeleted: {
                if (selectedPathList.size() == 0) {
                    Toast.makeText(this, "Please select at least one file", Toast.LENGTH_SHORT).show();
                    return;
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(R.string.str_msg_delet_file);
                builder.setPositiveButton("Yes", (dialog, which) -> deleteSelectedFile());
                builder.setNegativeButton("No", (dialog, which) -> dialog.dismiss());
                AlertDialog dialog =  builder.create();
                dialog.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.back_white));
                dialog.show();
                break;
            }
            case R.id.btnSelectAll: {
                ((AppCompatImageView) findViewById(R.id.ivMenu)).setColorFilter(ContextCompat.getColor(this, R.color.colorBlue), android.graphics.PorterDuff.Mode.SRC_IN);
                selectAllFiles();
                groupBtn.setVisibility(View.VISIBLE);
                break;
            }
            case R.id.btnUnSelectAll: {
                ((AppCompatImageView) findViewById(R.id.ivMenu)).setColorFilter(ContextCompat.getColor(this, R.color.black), PorterDuff.Mode.SRC_IN);
                unSelectAllFiles();
                groupBtn.setVisibility(View.GONE);
            }
        }
    }

    private void unSelectAllFiles() {
        selectedPathList.clear();
        selectedPositionList.clear();
        for (int i = 0; i < imageList.size(); i++) {
            imageAdapter.imageObjectList.get(i).setCheck(false);
        }
        imageAdapter.notifyDataSetChanged();
    }

    private void selectAllFiles() {
        selectedPathList.clear();
        selectedPositionList.clear();
        for (int i = 0; i < imageList.size(); i++) {
            imageAdapter.imageObjectList.get(i).setCheck(true);
            selectedPathList.add(imageList.get(i).getPath());
            selectedPositionList.add(i);
        }
        imageAdapter.notifyDataSetChanged();
    }

    private void deleteSelectedFile() {
        Collections.sort(selectedPositionList);
        Iterator<String> it1 = selectedPathList.iterator();
        int i = 0;
        while (it1.hasNext()) {
            String next = it1.next();
            File file = new File(next);
            if (!file.delete())
                Toast.makeText(this, getText(R.string.str_file_can_not_deleted), Toast.LENGTH_SHORT).show();

            folderList.remove(next);
            imageList.remove(selectedPositionList.get(i) - i);
            savedSelectedPathList.add(next);
            i++;
        }
        isClicked = true;
        imageAdapter.notifyDataSetChanged();
        String msg;
        if (i == 1)
            msg = i + " " + "photo deleted";
        else
            msg = i + " " + "photos deleted";

        Toast.makeText(PhotoFolderActivity.this, msg, Toast.LENGTH_LONG).show();
        selectedPositionList.clear();
        selectedPathList.clear();
        groupBtn.setVisibility(View.GONE);
    }

    private void findViews() {

        ((Toolbar) findViewById(R.id.toolbar)).setNavigationIcon(R.drawable.ic_back);
        ((Toolbar) findViewById(R.id.toolbar)).setNavigationOnClickListener(v -> onBackPressed());
        ((TextView) findViewById(R.id.tvTitle)).setText(R.string.deleted_image_recovery);

        groupBtn = findViewById(R.id.groupBtn);
        progressBar = findViewById(R.id.progressBar);
        findViewById(R.id.ivMenu).setVisibility(View.GONE);
    }

    private void initViews() {
        int intExtra = getIntent().getIntExtra("position", 0);
        ArrayList<String> arrayList = PhotoListActivity.imagePathList.get(intExtra);
        folderList = arrayList;
        for (String s : arrayList) {
            imageList.add(new ImageObject(s, false));
        }
        folderName = PhotoListActivity.folderList.get(intExtra);
        imageAdapter = new ImageAdapter(this, imageList);
        folderSize = imageList.size();

        ((RecyclerView) findViewById(R.id.recyclerview)).setAdapter(imageAdapter);
    }

    @Override
    public void onBackPressed() {

        if (!isClicked)
            selectedPathList.clear();
        if (imageAdapter != null)
            imageAdapter = null;

        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
        finish();

    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        if (!isClicked) {
            selectedPathList.clear();
        }
        Intent intent = new Intent("Deleted Paths");
        intent.putExtra("Deleted List", savedSelectedPathList);
        intent.putExtra("folder_name", folderName);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        finish();
        return true;
    }

    @SuppressLint("StaticFieldLeak")
    public class RestoreFileTask extends AsyncTask<Void, Void, Void> {

        public Void restoreFile() {
            Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            try {
                for (String selectedPath : selectedPathList) {
                    String[] split = selectedPath.split("/");
                    File selectedFile = new File(selectedPath);
                    String str = "";
                    if (!split[split.length - 1].contains(".")) {
                        if (Utils.isJPEG(selectedFile)) {
                            str = ".jpg";
                        } else if (Utils.isPNG(selectedFile)) {
                            str = ".png";
                        }
                    }
                    String BACKUP_PHOTO_FOLDER = Constant.BASE_PATH + getString(R.string.app_name) + File.separator + getString(R.string.photo) + File.separator;
                    File restoreFile = new File(BACKUP_PHOTO_FOLDER + split[split.length - 1] + str);
                    try {
                        Utils.copyDirectory(PhotoFolderActivity.this, selectedFile, restoreFile);
                        intent.setData(Uri.fromFile(restoreFile));
                        sendBroadcast(intent);
                        countCopied++;
                        publishProgress();
                    } catch (Exception e) {
                        countError++;
                        e.printStackTrace();
                    }
                }
                return null;
            } catch (Exception unused) {
                return null;
            }
        }

        @Override
        public Void doInBackground(Void[] voidArr) {
            return restoreFile();
        }


        @Override
        @SuppressLint({"WrongConstant", "NotifyDataSetChanged"})
        public void onPostExecute(Void r3) {
            super.onPostExecute(r3);

            countError = folderSize - countCopied;
            selectedPathList.clear();

            countCopied = 0;
            countError = 0;

            groupBtn.setVisibility(View.GONE);
            for (int i = 0; i < imageList.size(); i++) {
                imageAdapter.imageObjectList.get(i).setCheck(false);
            }
            imageAdapter.notifyDataSetChanged();

            new Handler().postDelayed(() -> {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(PhotoFolderActivity.this, "Image successfully recovered.", Toast.LENGTH_LONG).show();
            }, 500);

        }

        public void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        public void onProgressUpdate(Void[] voidArr) {
            super.onProgressUpdate(voidArr);
        }
    }

    public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ViewHolder> {
        public final TypedValue typedValue = new TypedValue();
        public int background;
        public List<ImageObject> imageObjectList;

        public ImageAdapter(Context context, List<ImageObject> list) {
            context.getTheme().resolveAttribute(R.attr.selectableItemBackground, typedValue, true);
            background = this.typedValue.resourceId;
            imageObjectList = list;
        }

        @Override
        public int getItemCount() {
            return imageObjectList.size();
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_image, viewGroup, false);
            inflate.setBackgroundResource(background);
            return new ViewHolder(inflate);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {

            if (imageList.get(position).isCheck()) {
                viewHolder.ivSelect.setImageDrawable(ContextCompat.getDrawable(viewHolder.itemView.getContext(), R.drawable.ic_select));
                viewHolder.ivMain.setAlpha(0.65f);
            } else {
                viewHolder.ivSelect.setImageDrawable(null);
                viewHolder.ivMain.setAlpha(1f);
            }

            viewHolder.ivMain.setOnClickListener(view -> {
                if (imageObjectList.get(position).isCheck()) {
                    imageObjectList.get(position).setCheck(false);
                    viewHolder.ivSelect.setImageDrawable(null);
                    viewHolder.ivMain.setAlpha(1.0f);
                    selectedPathList.remove(folderList.get(position));
                    selectedPositionList.remove(Integer.valueOf(position));
                } else {
                    imageObjectList.get(position).setCheck(true);
                    viewHolder.ivSelect.setImageDrawable(ContextCompat.getDrawable(view.getContext(), R.drawable.ic_select));
                    viewHolder.ivMain.setAlpha(0.65f);
                    selectedPathList.add(folderList.get(position));
                    selectedPositionList.add(position);
                }
                //btn visible
                if (selectedPositionList.size() == 0)
                    groupBtn.setVisibility(View.GONE);
                else
                    groupBtn.setVisibility(View.VISIBLE);

                isClicked = false;
            });

            Glide.with(viewHolder.ivMain.getContext())
                    .load(imageObjectList.get(position).getPath())
                    .placeholder(R.drawable.ic_loading_image)
                    .fitCenter()
                    .into(viewHolder.ivMain);

        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public final ImageView ivSelect;
            public final ImageView ivMain;

            public ViewHolder(View view) {
                super(view);
                ivMain = view.findViewById(R.id.image);

                ivSelect = view.findViewById(R.id.ivSelect);
                ivSelect.setVisibility(View.VISIBLE);
                ivSelect.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                view.findViewById(R.id.tvFolderCount).setVisibility(View.INVISIBLE);
            }

        }
    }

}