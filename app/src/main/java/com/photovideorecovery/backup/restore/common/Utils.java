package com.photovideorecovery.backup.restore.common;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.photovideorecovery.backup.restore.R;
import com.photovideorecovery.backup.restore.activity.ScanPhotoActivity;
import com.photovideorecovery.backup.restore.extension.StringBuilderKt;
import com.photovideorecovery.backup.restore.model.RestoreModel;
import com.photovideorecovery.backup.restore.utils.Constant;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class Utils {

    public static int MIN_FILE_SIZE = 100;
    public static String[] encrypted_list = {"hm+rh7Ztxrw=", "6c5e9lUyL8o=", "aah+a7EdFuA=", "32lkJZPb2gqqdfgieUgdoA==", "SfYAlLKlOAM=", "u20z4bmZHkA=", "GvybyhO4CSQ=", "7z63YuObWdaNZqeoXZf3C2WMQ0iB2eDERkPOi5ouYlEELTgX6SqWghH6xyt1YY7jG9Q/WIWySx4=", "bIEUQPAYQIA=", "vn2E9WISPdg=", "uXgF97JlScM=", "5RtC1SSHstZhNx5o5dJJenfmf37Xwxr0"};

    @NonNull
    public static HashSet<String> getExternalMounts() {
        HashSet<String> hashSet = new HashSet<>();

        String[] split = "".split("\n");
        for (String str2 : split) {
            if (!str2.toLowerCase(Locale.US).contains(string_decrypt(encrypted_list[9])) && str2.matches("(?i).*vold.*(vfat|ntfs|exfat|fat32|ext3|ext4).*rw.*")) {
                String[] split2 = str2.split(" ");
                for (String str3 : split2) {
                    if (str3.startsWith("/") && !str3.toLowerCase(Locale.US).contains(string_decrypt(encrypted_list[10]))) {
                        hashSet.add(str3);
                    }
                }
            }
        }
        hashSet.add(Environment.getExternalStorageDirectory().getAbsolutePath());
        return hashSet;
    }

    public static boolean isImageFile(File file) {
        if (file != null) {
            try {
                if (file.exists()) {
                    file.setReadable(true);
                    String name = file.getName();
                    if (!name.toLowerCase().contains(string_decrypt(encrypted_list[0])) && !name.toLowerCase().contains(string_decrypt(encrypted_list[1])) && !name.toLowerCase().contains(string_decrypt(encrypted_list[2])) && !name.toLowerCase().contains(string_decrypt(encrypted_list[3])) && !name.toLowerCase().contains(string_decrypt(encrypted_list[4])) && !name.toLowerCase().contains(string_decrypt(encrypted_list[5]))) {
                        if (!name.toLowerCase().contains(string_decrypt(encrypted_list[6]))) {
                            if (!name.contains(".")) {
                                if (file.length() > ((long) MIN_FILE_SIZE)) {
                                    try {
                                        if (isJPEG(file) || isPNG(file)) {
                                            return true;
                                        }
                                    } catch (Exception unused) {
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                    if (file.length() > ((long) MIN_FILE_SIZE)) {
                        if (isJPEG(file) || isPNG(file)) {
                            return true;
                        }
                    }
                }
            } catch (Exception unused2) {
                unused2.printStackTrace();
            }
        }
        return false;
    }

    @NonNull
    public static Boolean isJPEG(@NonNull File file) {
        String path = file.getAbsolutePath();
        String extension = path.substring(path.lastIndexOf("."));
        return extension.equals(".jpg");
    }

    public static boolean isPNG(@NonNull File file) {
        String path = file.getAbsolutePath();
        String extension = path.substring(path.lastIndexOf("."));
        return extension.equals(".png");
    }

    public static boolean isVideoFile(File file) {
        if (file != null) {
            try {
                if (file.exists()) {

                    String name = file.getName();
                    if ((checkVideoExtension(file, ".mp4")
                            || checkVideoExtension(file, ".avi")
                            || checkVideoExtension(file, ".mkv")
                            || checkVideoExtension(file, ".flv")
                            || checkVideoExtension(file, ".gif")
                            || checkVideoExtension(file, ".3gp"))) {
                        return name.contains(".") && file.length() > ((long) MIN_FILE_SIZE);
                    }
                }
            } catch (Exception unused) {
                return false;
            }
        }
        return false;
    }

    public static boolean checkVideoExtension(@NonNull File file, String ext) {
        String path = file.getAbsolutePath();
        String extension = path.substring(path.lastIndexOf("."));
        return extension.equals(ext);
    }

    @NonNull
    public static String string_decrypt(String str) {
        try {
            return new String(DES.decrypt(Base64.decode(str), ScanPhotoActivity.PACKAGE.getBytes()));
        } catch (Exception unused) {
            return "";
        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void copyDirectory(Context context, @NonNull File currentFile, File restoreFile) {
        try {
            if (!currentFile.isDirectory()) {
                File parentFile = restoreFile.getParentFile();

                if (parentFile == null || parentFile.exists() || parentFile.mkdirs()) {
                    FileInputStream fileInputStream = new FileInputStream(currentFile);
                    FileOutputStream fileOutputStream = new FileOutputStream(restoreFile);
                    byte[] bArr = new byte[1024];
                    while (true) {
                        int read = fileInputStream.read(bArr);
                        if (read > 0) {
                            fileOutputStream.write(bArr, 0, read);
                        } else {
                            fileInputStream.close();
                            fileOutputStream.close();
                            return;
                        }
                    }
                } else {
                    StringBuilder stringBuilder = StringBuilderKt.createBuilder(context.getString(R.string.str_cannot_create_folder));
                    stringBuilder.append(parentFile.getAbsolutePath());
                    throw new IOException(stringBuilder.toString());
                }
            } else if (restoreFile.exists() || restoreFile.mkdirs()) {
                String[] list = currentFile.list();
                if (list != null) {
                    for (String fileList : list) {
                        copyDirectory(context, new File(currentFile, fileList), new File(restoreFile, fileList));
                    }
                }
            } else {
                StringBuilder h2 = StringBuilderKt.createBuilder(context.getString(R.string.str_cannot_create_folder));
                h2.append(restoreFile.getAbsolutePath());
                throw new IOException(h2.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @NonNull
    public static List<RestoreModel> loadBackupApp(Context context) {
        ArrayList<RestoreModel> appList = new ArrayList<>();
        File file = new File(Constant.BACKUP_FOLDER);
        if (file.exists() && file.isDirectory())
            for (File file1 : Objects.requireNonNull(file.listFiles())) {
                if (file1.length() > 0L && file1.getPath().endsWith(".apk")) {
                    String str = file1.getPath();
                    PackageInfo packageInfo = context.getPackageManager().getPackageArchiveInfo(str, PackageManager.GET_ACTIVITIES);
                    if (packageInfo != null) {
                        ApplicationInfo applicationInfo = packageInfo.applicationInfo;
                        applicationInfo.sourceDir = str;
                        applicationInfo.publicSourceDir = str;
                        Drawable drawable = applicationInfo.loadIcon(context.getPackageManager());
                        RestoreModel restoreModel = new RestoreModel();
                        restoreModel.setIcon(drawable);
                        restoreModel.setFile(file1);
                        restoreModel.setPath(str);
                        restoreModel.setName(file1.getName());
                        appList.add(restoreModel);
                    }
                }
            }

        return appList;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        @SuppressLint("MissingPermission") NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
