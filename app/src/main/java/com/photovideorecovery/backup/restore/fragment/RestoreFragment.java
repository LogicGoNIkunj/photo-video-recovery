package com.photovideorecovery.backup.restore.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.photovideorecovery.backup.restore.R;
import com.photovideorecovery.backup.restore.activity.BackupAppActivity;
import com.photovideorecovery.backup.restore.adapter.BackAppAdapter;
import com.photovideorecovery.backup.restore.model.BackupModel;
import com.photovideorecovery.backup.restore.utils.Constant;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class RestoreFragment extends Fragment {

    public boolean taskRunning = false;

    public BackAppAdapter backupListAdapter = new BackAppAdapter();
    public PackageManager packageManager;
    public ProgressBar progressBar;
    public RecyclerView rvBackupApp;

    @Nullable
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.fragment_backup, viewGroup, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.rvBackupApp = view.findViewById(R.id.rvBackupApp);
        this.progressBar = view.findViewById(R.id.progressBar1);

        backupListAdapter.setListener(this::dialogAppOption);

        this.packageManager = requireActivity().getPackageManager();
    }

    @Override
    public void onResume() {
        super.onResume();
        refresh();
    }

    public void refresh() {
        if (this.taskRunning) {
            Toast.makeText(requireActivity(), "Task still running", Toast.LENGTH_SHORT).show();
        } else {
            new AppListLoaderTask().execute();
        }
    }

    public void dialogAppOption(BackupModel item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity(), R.style.AlertDialogTheme);
        ListView listView2 = new ListView(getActivity());
        listView2.setPadding(25, 25, 25, 25);
        listView2.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, new String[]{"Backup", "Uninstall", "Details"}));
        builder.setView(listView2);
        final AlertDialog create = builder.create();
        create.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(requireContext(),R.drawable.back_white));
        listView2.setOnItemClickListener((adapterView, view, i1, j) -> {
            create.dismiss();
            ArrayList<BackupModel> arrayList = new ArrayList<>();
            arrayList.add(item);
            switch (i1) {
                case 0:
                    new FileSaveTask(arrayList).execute();
                    return;
                case 1:
                    uninstallApp(arrayList);
                    return;
                case 2:
                    showInstalledAppDetails(item.getPackageName());
                    return;
                default:
            }
        });

        create.show();
    }

    public void uninstallApp(List<BackupModel> list) {
        for (BackupModel packageName : list) {
            Intent intent = new Intent(Intent.ACTION_UNINSTALL_PACKAGE);
            intent.setData(Uri.parse("package:" + packageName.getPackageName()));
            intent.putExtra(Intent.EXTRA_RETURN_RESULT, true);
            startActivityForResult(intent, Constant.UNINSTALL_REQUEST_CODE);
        }
    }

    public void showInstalledAppDetails(String str) {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setData(Uri.parse("package:" + str));
        startActivity(intent);
    }

    @Override
    public void onDestroy() {
        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
        super.onDestroy();
    }

    @SuppressLint("StaticFieldLeak")
    private class AppListLoaderTask extends AsyncTask<String, String, String> {
        private final ArrayList<BackupModel> appList = new ArrayList<>();
        private String status = "";

        public AppListLoaderTask() {
            taskRunning = true;
        }


        public void onPreExecute() {

            this.appList.clear();
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        public String doInBackground(String... strArr) {
            try {
                List<PackageInfo> installedPackages = packageManager.getInstalledPackages(0);
                for (int i = 0; i < installedPackages.size(); i++) {
                    PackageInfo packageInfo = installedPackages.get(i);
                    if ((packageInfo.applicationInfo.flags & 1) == 0) {
                        if (packageInfo.packageName!=null && !packageInfo.packageName.equalsIgnoreCase("")
                        && !packageInfo.packageName.equalsIgnoreCase(requireContext().getPackageName())) {
                            BackupModel backupModel = new BackupModel();
                            backupModel.setAppName(packageInfo.applicationInfo.loadLabel(packageManager).toString());
                            backupModel.setPackageName(packageInfo.packageName);
                            backupModel.setVersionName(packageInfo.versionName!=null?packageInfo.versionName:"");
                            backupModel.setAppIcon(packageInfo.applicationInfo.loadIcon(packageManager));
                            backupModel.setFile(new File(packageInfo.applicationInfo.publicSourceDir));
                            this.appList.add(backupModel);
                        }

                    }
                }
                this.status = "success";
            } catch (Exception unused) {
                this.status = "failed";
            }

            publishProgress();
            return null;
        }

        public void onProgressUpdate(String... strArr) {

            new Handler().postDelayed(() -> {
                if (this.status.equals("success")) {
                    taskRunning = false;
                    progressBar.setVisibility(View.GONE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        Collections.sort(this.appList, Comparator.comparing(backupModel -> backupModel.getAppName().toLowerCase()));
                    }

                    rvBackupApp.setAdapter(backupListAdapter);
                    backupListAdapter.setAppList(appList);
                } else {
                    Toast.makeText(requireActivity(), "Failed load applications", Toast.LENGTH_SHORT).show();
                }
            }, 500);

            super.onProgressUpdate(strArr);
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class FileSaveTask extends AsyncTask<Void, Integer, File> {

        private final List<BackupModel> selected_app;

        public FileSaveTask(List<BackupModel> list) {
            this.selected_app = list;
        }

        public void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
        }

        public File doInBackground(Void... voidArr) {
            File file = null;
            int i = 0;
            while (this.selected_app.size() > i) {
                String str = this.selected_app.get(i).getAppName() + "_" + this.selected_app.get(i).getVersionName() + ".apk";
                File file2 = new File(Constant.BACKUP_FOLDER);
                if (!file2.exists()) {
                    if (!file2.mkdirs()) {
                        return null;
                    }
                }
                File file3 = new File(file2.getPath() + "/" + str);
                try {
                    if (file3.exists()) {
                        file3.delete();
                    }
                    if (!file3.createNewFile()) {
                        return null;
                    }
                    FileInputStream fileInputStream = new FileInputStream(this.selected_app.get(i).getFile());
                    FileOutputStream fileOutputStream = new FileOutputStream(file3);
                    byte[] bArr = new byte[1024];
                    while (true) {
                        int read = fileInputStream.read(bArr);
                        if (read <= 0) {
                            break;
                        }
                        fileOutputStream.write(bArr, 0, read);
                    }
                    fileInputStream.close();
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                i++;
                file = file2;
            }
            return file;
        }


        public void onPostExecute(File file) {

            new Handler().postDelayed(() -> {
                progressBar.setVisibility(View.GONE);
                if (file != null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity(), R.style.AlertDialogTheme);
                    builder.setCancelable(false);
                    builder.setTitle("Backup Completed");
                    builder.setMessage("APK Location: " + Constant.BACKUP_FOLDER);
                    builder.setPositiveButton("OK", (dialogInterface, i) -> {
                        dialogInterface.dismiss();
                        Objects.requireNonNull(((BackupAppActivity) requireActivity()).backupFragment).refreshList();
                    });
                    AlertDialog dialog = builder.create();
                    dialog.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.back_white));
                    dialog.show();
                    return;
                }
                Toast.makeText(getActivity(), "APK backup failed", Toast.LENGTH_LONG).show();
            }, 1000);
        }
    }

}
