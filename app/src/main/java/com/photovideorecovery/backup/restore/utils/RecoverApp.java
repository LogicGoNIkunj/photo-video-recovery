package com.photovideorecovery.backup.restore.utils;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.appcompat.app.AppCompatDelegate;

import com.photovideorecovery.backup.restore.R;

import retrofit2.Retrofit;

public class RecoverApp extends Application {

    @SuppressLint("StaticFieldLeak")
    public static AppOpenManager mAppOpenManager;
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;
    @SuppressLint("StaticFieldLeak")
    private static Context context;
    public static Retrofit retrofit;




    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        sharedPreferences = getSharedPreferences("ps", MODE_PRIVATE);
        editor = sharedPreferences.edit();
        mAppOpenManager = new AppOpenManager(this);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public static void set_AdsInt(int adsInt) {
        editor.putInt("adsInt",adsInt).commit();
    }

    public static int get_AdsInt() {
        return sharedPreferences.getInt("adsInt",3);
    }

    //admob interstitial
    public static void set_Admob_interstitial_Id(String Admob_interstitial_Id) {
        editor.putString("Admob_interstitial_Id",Admob_interstitial_Id).commit();
    }

    public static String get_Admob_interstitial_Id(){
        return sharedPreferences.getString("Admob_interstitial_Id", context.getString(R.string.admob_Interstitial_ad));
    }

    //admob native
    public static void set_Admob_native_Id(  String Admob_native_Id) {
        editor.putString("Admob_native_Id",Admob_native_Id).commit();
    }

    public static String get_Admob_native_Id()  {
        return sharedPreferences.getString("Admob_native_Id",context.getString(R.string.    admob_native_ad_key));
    }
    //admob openapp
    public static void set_Admob_openapp(String Admob_native_Id) {
        editor.putString("Admob_open_Id", Admob_native_Id).commit();
    }

    public static String get_Admob_openapp() {
        return sharedPreferences.getString("Admob_open_Id", context.getString(R.string.open_app_ad_id));
    }


    //qureka
    public static void set_qureka(boolean yes) {
        editor.putBoolean("qureka_Id", yes).commit();
    }

    public static boolean get_qureka() {
        return sharedPreferences.getBoolean("qureka_Id", false);
    }


}
