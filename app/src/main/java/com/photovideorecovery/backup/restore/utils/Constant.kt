package com.photovideorecovery.backup.restore.utils

import android.Manifest
import android.os.Environment
import java.io.File

object Constant {

    const val UNINSTALL_REQUEST_CODE: Int = 1001
    const val IS_PERMISSION_SCREEN_SHOWED: String = "is_first_time_image_video_recovery"
    const val PREFERENCE_MANAGER: String = "preference_image_video_app_contact_recovery"

    @JvmField
    var BASE_PATH = Environment.getExternalStorageDirectory().toString() + File.separator

    @JvmField
    var BACKUP_FOLDER = ""

    const val PERMISSION_CODE = 1001
    const val MANAGE_PERMISSION_CODE = 1002
    val PERMISSION = arrayOf(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_PHONE_STATE,
        Manifest.permission.READ_CONTACTS,
        Manifest.permission.WRITE_CONTACTS
    )

}