package com.photovideorecovery.backup.restore.model;

public class ImageObject {

    public boolean isCheck;
    public String filePath;

    public ImageObject(String str, boolean isCheck) {
        this.filePath = str;
        this.isCheck = isCheck;
    }

    public String getPath() {
        return this.filePath;
    }

    public boolean isCheck() {
        return this.isCheck;
    }

    public void setCheck(boolean isCheck) {
        this.isCheck = isCheck;
    }

}
