package com.photovideorecovery.backup.restore.diffutils

import androidx.recyclerview.widget.DiffUtil
import com.photovideorecovery.backup.restore.model.BackupModel

class BackAppDiffUtils(
    private val oldList: ArrayList<BackupModel>,
    private val newList: ArrayList<BackupModel>
) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] === newList[newItemPosition]
    }
}