package com.photovideorecovery.backup.restore.adapter;


import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class FragmentAdapter extends FragmentPagerAdapter {
    private final List<String> mFragmentTitles = new ArrayList<>();
    private final List<Fragment> mFragments = new ArrayList<>();

    public FragmentAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    public void addFragment(Fragment fragment, String str) {
        this.mFragments.add(fragment);
        this.mFragmentTitles.add(str);
    }

    @NonNull
    @Override
    public Fragment getItem(int i) {
        return this.mFragments.get(i);
    }

    @Override
    public int getCount() {
        return this.mFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int i) {
        return this.mFragmentTitles.get(i);
    }
}
