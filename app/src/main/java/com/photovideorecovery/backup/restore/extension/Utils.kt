package com.photovideorecovery.backup.restore.extension

import android.content.Context
import android.support.v4.media.session.PlaybackStateCompat
import android.widget.Toast
import java.util.*

fun Long.convertStorage(): String {
    return when {
        this >= 1073741824 -> {
            String.format(Locale.getDefault(), "%.1f GB", this.toFloat() / 1.07374182E9f)
        }
        this >= PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED -> {
            val mb = this.toFloat() / 1048576.0f
            String.format(if (mb > 100.0f) "%.0f MB" else "%.1f MB", mb)
        }
        this >= PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID -> {
            val kb = this.toFloat() / 1024.0f
            String.format(if (kb > 100.0f) "%.0f KB" else "%.1f KB", kb)
        }
        else -> {
            String.format(Locale.getDefault(), "%d B", this)
        }
    }
}

fun Context.showToast(msg: String) = Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()