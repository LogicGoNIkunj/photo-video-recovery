package com.photovideorecovery.backup.restore.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.photovideorecovery.backup.restore.databinding.BottomDialogBinding

class BottomSheetDialogFragment(
    private val title: String,
    private val msg: String,
    private val positiveButtonClick: String,
    private val negativeButtonClick: String,
    private val dialogInterface: DialogInterface
) : BottomSheetDialogFragment() {


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        isCancelable = false
        val binding = BottomDialogBinding.inflate(inflater)

        binding.tvTitle.text = title
        binding.tvMessage.text = msg
        binding.btnPositive.text = positiveButtonClick
        binding.btnNegative.text = negativeButtonClick
        binding.btnPositive.setOnClickListener {
            dialogInterface.onPositiveClick()
            dismiss()
        }
        binding.btnNegative.setOnClickListener {
            dialogInterface.onNegativeClick()
            dismiss()
        }


        return binding.root
    }


}