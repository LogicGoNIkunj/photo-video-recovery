package com.photovideorecovery.backup.restore.extension

import android.content.res.ColorStateList
import android.view.View
import androidx.annotation.ColorRes

fun View.show() { this.visibility = View.VISIBLE }

fun View.hide() { this.visibility = View.GONE }

fun View.invisible()  { this.visibility = View.INVISIBLE }

internal fun View.changeTint(@ColorRes color: Int){
    backgroundTintList = ColorStateList.valueOf(context.getCompactColor(color))
}
