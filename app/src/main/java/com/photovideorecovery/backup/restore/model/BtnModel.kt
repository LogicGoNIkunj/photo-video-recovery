package com.photovideorecovery.backup.restore.model

class BtnModel {
    var status = false
    var message = ""
    var data: Data? = null
}

class Data {
    var title: String = ""
    var url: String = ""
    var image: String = ""
    var flage = false
}