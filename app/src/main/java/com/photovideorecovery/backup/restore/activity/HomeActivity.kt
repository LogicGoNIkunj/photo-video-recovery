package com.photovideorecovery.backup.restore.activity

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.graphics.Typeface
import android.net.Uri
import android.os.*
import android.text.SpannableStringBuilder
import android.text.style.RelativeSizeSpan
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.bumptech.glide.Glide
import com.google.android.gms.ads.*
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import com.google.android.gms.ads.nativead.MediaView
import com.google.android.gms.ads.nativead.NativeAd
import com.google.android.gms.ads.nativead.NativeAdView
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.onesignal.OneSignal
import com.photovideorecovery.backup.restore.BuildConfig
import com.photovideorecovery.backup.restore.R
import com.photovideorecovery.backup.restore.activity.SplashActivity.Companion.publisher_yes
import com.photovideorecovery.backup.restore.api_manager.API
import com.photovideorecovery.backup.restore.api_manager.Model_Playdata
import com.photovideorecovery.backup.restore.common.Utils.isNetworkAvailable
import com.photovideorecovery.backup.restore.extension.*
import com.photovideorecovery.backup.restore.fragment.FeedbackDialogFragment
import com.photovideorecovery.backup.restore.utils.CustomTypefaceSpan
import com.photovideorecovery.backup.restore.utils.RecoverApp
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit

class HomeActivity : AppCompatActivity(R.layout.activity_home) {

    private var mInterstitialAd: InterstitialAd? = null
    var qurekaimage = ""
    var querekatext: String? = ""
    var url: String? = ""
    var checkqureka = false
    private var isActivityLeft = false


    override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)
        isActivityLeft = false
        analytics()
        RecoverApp.mAppOpenManager!!.isAdShow = true
        loadAds()
        admobBigNative(findViewById(R.id.flNativeAdd))
        initToolbar()
        GetData()
    }

    @SuppressLint("MissingPermission")
    fun analytics() {
        MobileAds.initialize(this)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        FirebaseAnalytics.getInstance(this)
        FirebaseCrashlytics.getInstance()
        OneSignal.initWithContext(this)
        OneSignal.setAppId(resources.getString(R.string.one_signal_id))
    }


    @SuppressLint("MissingPermission")
    private fun admobBigNative(admob_native_ll: FrameLayout) {
        val builder = AdLoader.Builder(this, RecoverApp.get_Admob_native_Id())
            .forNativeAd { nativeAd: NativeAd ->
                @SuppressLint("InflateParams") val adView =
                    layoutInflater.inflate(R.layout.layout_unified_native_ad, null) as NativeAdView
                populateUnifiedNativeAdView(nativeAd, adView)

                findViewById<AppCompatImageView>(R.id.appCompatImageView).hide()
                admob_native_ll.removeAllViews()
                admob_native_ll.addView(adView)
            }

        val adLoader = builder.withAdListener(object : AdListener() {
            override fun onAdFailedToLoad(loadAdError: LoadAdError) {
                super.onAdFailedToLoad(loadAdError)
                findViewById<AppCompatImageView>(R.id.appCompatImageView).show()
            }
        }).build()

        adLoader.loadAd(AdRequest.Builder().build())
    }

    private fun populateUnifiedNativeAdView(nativeAd: NativeAd, adView: NativeAdView) {
        val mediaView: MediaView = adView.findViewById(R.id.ad_media)
        mediaView.setImageScaleType(ImageView.ScaleType.CENTER_CROP)
        adView.mediaView = mediaView
        adView.headlineView = adView.findViewById(R.id.ad_headline)
        adView.bodyView = adView.findViewById(R.id.ad_body)
        adView.callToActionView = adView.findViewById(R.id.ad_call_to_action)
        adView.iconView = adView.findViewById(R.id.ad_app_icon)
        (adView.headlineView as TextView).text = nativeAd.headline
        if (nativeAd.body == null) {
            adView.bodyView?.invisible()
        } else {
            adView.bodyView?.show()
            (adView.bodyView as TextView).text = nativeAd.body
        }
        if (nativeAd.callToAction == null) {
            adView.callToActionView?.invisible()
        } else {
            adView.callToActionView?.show()
            (adView.callToActionView as TextView).text = nativeAd.callToAction
        }
        if (nativeAd.icon == null) {
            adView.iconView?.hide()
        } else {
            (adView.iconView as ImageView).setImageDrawable(nativeAd.icon?.drawable)
            adView.iconView?.show()
        }
        adView.setNativeAd(nativeAd)
    }


    fun loadAds() {
        val adRequest = AdRequest.Builder().build()
        InterstitialAd.load(this, RecoverApp.get_Admob_interstitial_Id(), adRequest,
            object : InterstitialAdLoadCallback() {
                override fun onAdLoaded(interstitialAd: InterstitialAd) {
                    mInterstitialAd = interstitialAd
                    mInterstitialAd!!.fullScreenContentCallback = object :
                        FullScreenContentCallback() {
                        override fun onAdDismissedFullScreenContent() {
                            RecoverApp.mAppOpenManager?.isAdShow = false
                            mInterstitialAd = null
                            loadAds()
                        }

                        override fun onAdFailedToShowFullScreenContent(adError: AdError) {
                            RecoverApp.mAppOpenManager?.isAdShow = false
                        }

                        override fun onAdShowedFullScreenContent() {
                            mInterstitialAd = null
                            RecoverApp.mAppOpenManager?.isAdShow = true
                        }
                    }
                }

                override fun onAdFailedToLoad(loadAdError: LoadAdError) {
                    mInterstitialAd = null
                    RecoverApp.mAppOpenManager?.isAdShow = false
                }
            })
    }


    private fun initToolbar() {
        val drawerLayout = findViewById<DrawerLayout>(R.id.drawerLayout)
        val toggle: ActionBarDrawerToggle = object : ActionBarDrawerToggle(
            this,
            drawerLayout,
            findViewById(R.id.toolbar),
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        ) {}
        toggle.setHomeAsUpIndicator(ContextCompat.getDrawable(this, R.drawable.ic_hamburger_icon))
        toggle.isDrawerIndicatorEnabled = false
        drawerLayout.addDrawerListener(toggle)
        toggle.toolbarNavigationClickListener =
            View.OnClickListener { drawerLayout.openDrawer(GravityCompat.START) }
        toggle.syncState()

        //set setting navigation data
        val typefaceNormal =
            Typeface.create(ResourcesCompat.getFont(this, R.font.gilroy_regular), Typeface.NORMAL)
        var path = Environment.getExternalStorageDirectory()
            .toString() + File.separator + getString(R.string.app_name) + File.separator
        path = path.removeRange((path.length - 1) until path.length)
        val stringBuilderAmt = SpannableStringBuilder("\n" + path)
        stringBuilderAmt.setSpan(
            CustomTypefaceSpan("", typefaceNormal),
            0,
            stringBuilderAmt.length,
            0
        )
        stringBuilderAmt.setSpan(RelativeSizeSpan(0.8f), 0, stringBuilderAmt.length, 0)

        findViewById<AppCompatTextView>(R.id.navSetting).append(stringBuilderAmt)


    }

    fun onClick(view: View) {
        var intent: Intent? = null
        when (view.id) {
            R.id.navShare -> {
                navShareApp()
                findViewById<DrawerLayout>(R.id.drawerLayout).closeDrawer(GravityCompat.START)
                return
            }
            R.id.navRateApp -> {
                showRatingbarDialog()
                findViewById<DrawerLayout>(R.id.drawerLayout).closeDrawer(GravityCompat.START)
                return
            }
            R.id.navPrivacy -> {
                if (isNetworkAvailable(this)) {
                    startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse(resources.getString(R.string.privacy_policy_link))
                        )
                    )
                    findViewById<DrawerLayout>(R.id.drawerLayout).closeDrawer(GravityCompat.START)
                    return
                }
                showToast("Please turn ON mobile network")
                return
            }
            R.id.btnImageRecover -> {
                startActivity<ScanPhotoActivity> { }

                try {
                    if (mInterstitialAd != null && !isActivityLeft) {
                        mInterstitialAd!!.show(this)
                        RecoverApp.mAppOpenManager!!.isAdShow = true
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            R.id.btnVideoRecover -> {
                startActivity<ScanVideoActivity> { }
            }
            R.id.btnContactRecover -> {
                startActivity<ContactListActivity> { }
            }
            R.id.btnAppRecover -> {
                startActivity<BackupAppActivity> { }
                try {
                    if (mInterstitialAd != null && !isActivityLeft) {
                        mInterstitialAd!!.show(this)
                        RecoverApp.mAppOpenManager!!.isAdShow = true
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            R.id.btnRecovered -> {
                startActivity<RecoveredActivity> { }
                try {
                    if (mInterstitialAd != null && !isActivityLeft) {
                        mInterstitialAd!!.show(this)
                        RecoverApp.mAppOpenManager!!.isAdShow = true
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    private fun navShareApp() {
        try {
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name))
            var shareMessage =
                getString(R.string.app_name) + " is best recovery app for backup your photos, video, contacts and restore them. In this app also create copy of application.\n\n"
            shareMessage =
                "${shareMessage}https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}"
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
            startActivity(Intent.createChooser(shareIntent, "choose one"))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun showRatingbarDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(dialog.window?.attributes)
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT
        dialog.window?.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.back_white))
        dialog.setContentView(R.layout.dialog_rate_us)
        (dialog.findViewById<RatingBar>(R.id.rbAppRate)).setOnRatingBarChangeListener { ratingBar, _, _ ->

            if (ratingBar.rating.toDouble() == 0.0) {
                Toast.makeText(this@HomeActivity, "Please give some rating", Toast.LENGTH_SHORT)
                    .show()
                return@setOnRatingBarChangeListener
            }
            if (ratingBar.rating >= 4.0) {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("market://details?id=" + BuildConfig.APPLICATION_ID)
                    )
                )
            } else {
                val dialogFragment = FeedbackDialogFragment()
                dialogFragment.show(supportFragmentManager, "FeedbackDialogFragment")
            }
            dialog.dismiss()
        }

        dialog.findViewById<View>(R.id.ivClose).setOnClickListener { dialog.dismiss() }
        dialog.show()
    }


    override fun onResume() {
        super.onResume()
        isActivityLeft = false
    }

    override fun onPause() {
        super.onPause()
        this.isActivityLeft = true
    }

    override fun onStop() {
        super.onStop()
        this.isActivityLeft = true
    }

    override fun onDestroy() {
        super.onDestroy()
        isActivityLeft = true
    }

    override fun onBackPressed() {
        startActivity<Exit_Activity> { }
    }


    fun GetData() {
        try {
            if (isNetworkAvailable(this@HomeActivity)) {
                if (RecoverApp.retrofit == null) {
                    RecoverApp.retrofit = Retrofit.Builder().baseUrl(API.Qureka_API).client(
                        OkHttpClient.Builder().addInterceptor { chain: Interceptor.Chain ->
                            chain.proceed(
                                chain.request().newBuilder().build()
                            )
                        }.connectTimeout(100, TimeUnit.SECONDS).readTimeout(100, TimeUnit.SECONDS)
                            .build()
                    ).addConverterFactory(GsonConverterFactory.create()).build()
                }
                val nativeClient = RecoverApp.retrofit.create(API::class.java)
                nativeClient.btnAd.enqueue(object : Callback<Model_Playdata?> {
                    override fun onResponse(
                        call: Call<Model_Playdata?>,
                        response: Response<Model_Playdata?>
                    ) {
                        try {
                            if (response.body() != null) {
                                if (response.body()!!.isStatus) {
                                    if (response.body()!!.data != null) {
                                        checkqureka = response.body()!!.data.isFlage
                                        if (publisher_yes) {
                                            if (response.body()!!.data.isFlage) {
                                                if (RecoverApp.get_qureka()) {
                                                    qurekaimage = response.body()!!.data.image
                                                    querekatext = response.body()!!.data.title
                                                    url = response.body()!!.data.url
                                                    Glide.with(this@HomeActivity).load(qurekaimage)
                                                        .into(findViewById<View>(R.id.img) as ImageView)
                                                    findViewById<View>(R.id.qurekaads).visibility =
                                                        View.VISIBLE
                                                    findViewById<View>(R.id.qureka).setOnClickListener {
                                                        try {
                                                            val builder = CustomTabsIntent.Builder()
                                                            val customTabsIntent = builder.build()
                                                            customTabsIntent.intent.setPackage("com.android.chrome")
                                                            customTabsIntent.launchUrl(
                                                                this@HomeActivity,
                                                                Uri.parse(url)
                                                            )
                                                        } catch (e: Exception) {
                                                            showToast("Something went wrong")
                                                        }
                                                    }
                                                } else {
                                                    findViewById<View>(R.id.img).visibility =
                                                        View.GONE
                                                    findViewById<View>(R.id.qurekaads).visibility =
                                                        View.GONE
                                                }
                                            } else {
                                                findViewById<View>(R.id.img).visibility = View.GONE
                                                findViewById<View>(R.id.qurekaads).visibility =
                                                    View.GONE
                                            }
                                        }
                                    } else {
                                        findViewById<View>(R.id.img).visibility = View.GONE
                                        findViewById<View>(R.id.qurekaads).visibility = View.GONE
                                    }
                                }
                            }
                        } catch (exception: java.lang.Exception) {
                            exception.printStackTrace()
                        }
                    }

                    override fun onFailure(call: Call<Model_Playdata?>, t: Throwable) {
                        findViewById<View>(R.id.img).visibility = View.GONE
                        findViewById<View>(R.id.qurekaads).visibility = View.GONE

                    }
                })
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }
}