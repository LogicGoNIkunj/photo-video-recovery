package com.photovideorecovery.backup.restore.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.photovideorecovery.backup.restore.R
import com.photovideorecovery.backup.restore.diffutils.ContactDiffUtils
import com.photovideorecovery.backup.restore.model.Contact

class ContactsAdapter : RecyclerView.Adapter<ContactsAdapter.Holder>() {

    var listener: OnItemClickListener? = null

    var contactList = arrayListOf<Contact>()
        set(list) {
            contactList.clear()
            contactList.addAll(list)
            DiffUtil.calculateDiff(ContactDiffUtils(this.contactList, list)).dispatchUpdatesTo(this)
        }


    class Holder(item: View) : RecyclerView.ViewHolder(item) {
        var tvName: TextView = item.findViewById(R.id.txt_name)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_contact, parent, false)
        return Holder(view)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.tvName.text = contactList[position].getContactName()
        holder.itemView.setOnClickListener {
            listener?.itemClick(contactList[position])
        }
    }

    override fun getItemCount(): Int {
        return contactList.size
    }

    interface OnItemClickListener {
        fun itemClick (contact: Contact)
    }
}