package com.photovideorecovery.backup.restore.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.photovideorecovery.backup.restore.R
import com.photovideorecovery.backup.restore.adapter.FragmentAdapter
import com.photovideorecovery.backup.restore.databinding.ActivityBackUpAppBinding
import com.photovideorecovery.backup.restore.extension.getCompactColor
import com.photovideorecovery.backup.restore.extension.getCompactDrawable
import com.photovideorecovery.backup.restore.fragment.BackupFragment
import com.photovideorecovery.backup.restore.fragment.RestoreFragment
import com.photovideorecovery.backup.restore.utils.Constant
import java.io.File

class BackupAppActivity : AppCompatActivity() {
    private var restoreFragment: RestoreFragment? = null
    private var fragmentAdapter: FragmentAdapter? = null

    @JvmField
    var backupFragment: BackupFragment? = null

    private lateinit var binding: ActivityBackUpAppBinding
    public override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)
        binding = ActivityBackUpAppBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initView()
        initToolbar()
    }

    private fun initView() {
        Constant.BACKUP_FOLDER = Constant.BASE_PATH + getString(R.string.app_name) + File.separator
        fragmentAdapter = FragmentAdapter(supportFragmentManager)
        if (restoreFragment == null) restoreFragment = RestoreFragment()
        if (backupFragment == null) backupFragment = BackupFragment()
        fragmentAdapter?.addFragment(restoreFragment, getString(R.string.tab_title_restore))
        fragmentAdapter?.addFragment(backupFragment, getString(R.string.tab_title_backup))
        binding.viewPager.adapter = fragmentAdapter
        binding.viewPager.offscreenPageLimit = 2
        binding.viewPager.currentItem = 0
        binding.viewPager.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrolled(i: Int, positionOffset: Float, positionOffsetPixels: Int) =
                Unit


            override fun onPageSelected(position: Int) {
                if (position == 0) {
                    binding.btnBackup.setImageDrawable(getCompactDrawable(R.drawable.btn_backup_unpress))
                    binding.btnRestoreApp.setImageDrawable(getCompactDrawable(R.drawable.btn_restore))
                } else {
                    binding.btnBackup.setImageDrawable(getCompactDrawable(R.drawable.btn_backup))
                    binding.btnRestoreApp.setImageDrawable(getCompactDrawable(R.drawable.btn_restore_unpress))
                }
            }

            override fun onPageScrollStateChanged(state: Int) = Unit
        })
    }

    private fun initToolbar() {
        (findViewById<Toolbar>(R.id.toolbar)).setNavigationOnClickListener { onBackPressed() }
        window.addFlags(Int.MIN_VALUE)
        window.clearFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        window.statusBarColor = getCompactColor(R.color.colorPrimaryDark)
    }

    override fun onBackPressed() {
        if (fragmentAdapter != null) fragmentAdapter = null
        if (backupFragment != null) backupFragment = null
        if (restoreFragment != null) restoreFragment = null
        System.runFinalization()
        Runtime.getRuntime().gc()
        System.gc()
        finish()
    }

    @SuppressLint("NonConstantResourceId")
    fun onClick(view: View) {
        when (view.id) {
            R.id.btnBackup -> {
                binding.viewPager.currentItem = 1
            }
            R.id.btnRestoreApp -> {
                binding.viewPager.currentItem = 0
            }
            R.id.ivMenu -> {
                if (binding.viewPager.currentItem == 0)
                    restoreFragment?.refresh()
                else
                    backupFragment?.refreshList()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        backupFragment?.onActivityResult(requestCode, resultCode, data)
    }
}