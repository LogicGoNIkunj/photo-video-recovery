package com.photovideorecovery.backup.restore.fragment

import android.app.Activity
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.view.Display
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ProgressBar
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText
import com.photovideorecovery.backup.restore.BuildConfig
import com.photovideorecovery.backup.restore.R
import com.photovideorecovery.backup.restore.activity.HomeActivity
import com.photovideorecovery.backup.restore.api_manager.API
import com.photovideorecovery.backup.restore.api_manager.APIClient
import com.photovideorecovery.backup.restore.common.Utils
import com.photovideorecovery.backup.restore.extension.hide
import com.photovideorecovery.backup.restore.extension.invisible
import com.photovideorecovery.backup.restore.extension.show
import com.photovideorecovery.backup.restore.extension.showToast
import com.photovideorecovery.backup.restore.model.FeedbackModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class FeedbackDialogFragment : DialogFragment() {

    var progressBar: ProgressBar? = null
    var btnSend: MaterialButton? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_feedback, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() {
        val etTitle: TextInputEditText? = dialog?.findViewById(R.id.etTitle)
        val etDescription: TextInputEditText? = dialog?.findViewById(R.id.etDescription)
        val ivClose: AppCompatImageView? = dialog?.findViewById(R.id.ivClose)
        btnSend = dialog?.findViewById(R.id.btnSend)
        progressBar = dialog?.findViewById(R.id.progressBar)

        btnSend?.setOnClickListener {
            if (Utils.isNetworkAvailable(requireContext())) {
                when {
                    TextUtils.isEmpty(etTitle?.text) -> {
                        requireContext().showToast("Enter title")
                        etTitle?.requestFocus()
                    }
                    TextUtils.isEmpty(etDescription?.text) -> {
                        requireContext().showToast("Enter description")
                        etDescription?.requestFocus()
                    }
                    else -> {

                        hideKeyboard(etDescription)
                        var packageInfo: PackageInfo? = null
                        try {
                            packageInfo = requireActivity().packageManager.getPackageInfo(
                                BuildConfig.APPLICATION_ID,
                                0
                            )
                        } catch (e: PackageManager.NameNotFoundException) {
                            e.printStackTrace()
                        }
                        val title = etTitle?.text.toString()
                        val description = etDescription?.text.toString()
                        val appVersion: String =
                            packageInfo?.versionName ?: BuildConfig.VERSION_NAME
                        val appName = resources.getString(R.string.app_name)
                        val appVersionCode: Int = Build.VERSION.SDK_INT
                        val mobileModel: String = Build.MODEL

                        sendFeedBack(
                            appName,
                            title,
                            description,
                            mobileModel,
                            appVersionCode,
                            appVersion
                        )
                    }
                }
            } else {
                requireActivity().showToast(getString(R.string.check_your_network))
            }


        }
        ivClose?.setOnClickListener { dialog?.dismiss() }
    }

    private fun hideKeyboard(etDescription: TextInputEditText?) {
        val activity = (requireActivity() as HomeActivity)
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(etDescription?.windowToken, 0)
    }

    private fun sendFeedBack(
        appName: String,
        title: String,
        description: String,
        mobileModel: String,
        appVersionCode: Int,
        appVersion: String
    ) {

        val packageName: String = BuildConfig.APPLICATION_ID

        progressBar?.show()
        btnSend?.invisible()

        val api = APIClient.getClient().create(API::class.java)
        api.sendFeedback(
            appName,
            packageName,
            title,
            description,
            mobileModel,
            appVersionCode.toString(),
            appVersion
        ).enqueue(object : Callback<FeedbackModel> {
            override fun onResponse(
                call: Call<FeedbackModel>,
                response: Response<FeedbackModel>
            ) {
                if (response.code() == 200 && response.body() != null) {
                    if (response.body()?.status == true) {
                        progressBar?.hide()
                        btnSend?.show()
                        dialog?.dismiss()
                        requireActivity().showToast("Thank you for your feedback it is very helpful.")
                        return
                    }
                }

                progressBar?.hide()
                btnSend?.show()
                requireActivity().showToast("Please send feedback in play-store")
                dialog?.dismiss()
            }

            override fun onFailure(call: Call<FeedbackModel>, t: Throwable) {
                progressBar?.hide()
                btnSend?.show()
                requireActivity().showToast("Please send feedback in play-store")
                dialog?.dismiss()
            }
        })

    }

    override fun onStart() {
        super.onStart()

        val windowManager = requireActivity().windowManager
        val display: Display? = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R)
            requireActivity().display
        else
            @Suppress("DEPRECATION")
            windowManager.defaultDisplay


        val params = dialog?.window?.attributes
        @Suppress("DEPRECATION")
        params?.width = (display?.width?.times(0.9))?.toInt()
        dialog?.window?.attributes = params
        dialog?.window?.setBackgroundDrawable(
            ContextCompat.getDrawable(
                requireContext(),
                R.drawable.back_white
            )
        )

    }


}
