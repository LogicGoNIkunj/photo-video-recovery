package com.photovideorecovery.backup.restore.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.photovideorecovery.backup.restore.R;
import com.photovideorecovery.backup.restore.adapter.RestoreAppAdapter;
import com.photovideorecovery.backup.restore.common.Utils;
import com.photovideorecovery.backup.restore.model.RestoreModel;

import java.util.ArrayList;
import java.util.List;


public class BackupFragment extends Fragment {

    private final RestoreAppAdapter restoreListAdapter = new RestoreAppAdapter();
    private TextView tvNoDataFound;
    private RecyclerView rvRestoreApp;
    private ProgressBar progressBar;

    @Nullable
    public View onCreateView(@NonNull LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View view1 = layoutInflater.inflate(R.layout.fragment_restore, viewGroup, false);
        rvRestoreApp = view1.findViewById(R.id.rvRestoreApp);
        tvNoDataFound = view1.findViewById(R.id.tvNoDataFound);
        progressBar = view1.findViewById(R.id.progressBar);
        restoreListAdapter.setOnItemClickListener(this::dialogApkFileOption);
        return view1;
    }

    public void onResume() {
        super.onResume();
        new Handler().postDelayed(this::refreshList, 50);
    }

    public void refreshList() {
        tvNoDataFound.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        ArrayList<RestoreModel> apkList = (ArrayList<RestoreModel>) Utils.loadBackupApp(requireActivity());
        new Handler().postDelayed(() -> {
            if (getActivity() != null) {
                restoreListAdapter.setRestoreAppList(apkList);
                rvRestoreApp.setAdapter(restoreListAdapter);
                if (apkList.size() == 0)
                    tvNoDataFound.setVisibility(View.VISIBLE);
                else
                    tvNoDataFound.setVisibility(View.GONE);

                progressBar.setVisibility(View.GONE);
            }

        }, 500);
    }

    public void dialogApkFileOption(RestoreModel item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
        builder.setTitle("Apk File Option");
        ListView listView2 = new ListView(requireActivity());
        listView2.setPadding(25, 25, 25, 25);
        listView2.setAdapter(new ArrayAdapter<>(requireActivity(), android.R.layout.simple_list_item_1, new String[]{"Restore", "Share", "Delete file"}));
        builder.setView(listView2);
        final AlertDialog create = builder.create();
        create.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.back_white));
        listView2.setOnItemClickListener((adapterView, view, i1, j) -> {
            create.dismiss();
            ArrayList<RestoreModel> arrayList = new ArrayList<>();
            arrayList.add(item);
            switch (i1) {
                case 0:
                    restoreApkFiles(arrayList);
                    return;
                case 1:
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    Uri fromFile = Uri.fromFile(item.getFile());
                    if (Build.VERSION.SDK_INT >= 24)
                        fromFile = FileProvider.getUriForFile(requireActivity(), requireActivity().getPackageName()+".provider", item.getFile());

                    intent.putExtra(Intent.EXTRA_STREAM, fromFile);
                    intent.setType("*/*");
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    return;
                case 2: {
                    AlertDialog.Builder deleteBuilder = new AlertDialog.Builder(requireActivity());
                    deleteBuilder.setTitle("Delete App");
                    deleteBuilder.setMessage("Are you sure want to delete ?");

                    deleteBuilder.setPositiveButton("Yes", (dialogInterface, i) -> {
                        dialogInterface.dismiss();
                        deleteApkFiles(arrayList);
                        refreshList();
                    });
                    deleteBuilder.setNegativeButton("No", (dialogInterface, i) -> dialogInterface.dismiss());
                    final AlertDialog deleteCreate = deleteBuilder.create();
                    deleteCreate.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.back_white));
                    deleteCreate.show();
                    break;
                }
            }
        });
        create.show();
    }

    public void restoreApkFiles(@NonNull List<RestoreModel> list) {
        for (RestoreModel next : list) {
            Uri fromFile = Uri.fromFile(next.getFile());
            if (Build.VERSION.SDK_INT >= 24) {
                fromFile = FileProvider.getUriForFile(requireActivity(), requireActivity().getPackageName()+".provider", next.getFile());
            }
            Intent intent = new Intent(Intent.ACTION_VIEW, fromFile);
            intent.putExtra(Intent.EXTRA_NOT_UNKNOWN_SOURCE, true);
            intent.setDataAndType(fromFile, "application/vnd.android.package-archive");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(intent);
        }
    }

    public void deleteApkFiles(@NonNull List<RestoreModel> list) {
        for (RestoreModel next : list) {
            if (next.getFile().exists())
                if (!next.getFile().delete())
                    Toast.makeText(requireActivity(), next.getFile().getName() + " app not deleted.", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(requireActivity(), next.getFile().getName() + " app is deleted.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDestroy() {
        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
        super.onDestroy();
    }
}
