package com.photovideorecovery.backup.restore.model

import android.graphics.drawable.Drawable
import java.io.File

class BackupModel {
    var appIcon: Drawable? = null
        private set
    var appName: String = ""
    var file: File? = null
        set(file2) {
            field = file2
        }
    var packageName: String = ""
    var versionName: String = ""
    fun setAppIcon(drawable: Drawable) {
        appIcon = drawable
    }
}