package com.photovideorecovery.backup.restore.utils

import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ProcessLifecycleOwner
import com.google.android.gms.ads.AdError
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.FullScreenContentCallback
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.appopen.AppOpenAd
import com.google.android.gms.ads.appopen.AppOpenAd.AppOpenAdLoadCallback
import com.photovideorecovery.backup.restore.activity.HomeActivity
import com.photovideorecovery.backup.restore.activity.StartActivity
import com.photovideorecovery.backup.restore.common.Utils
import com.photovideorecovery.backup.restore.extension.checkManageStoragePermission
import com.photovideorecovery.backup.restore.extension.startActivity
import com.photovideorecovery.backup.restore.utils.RecoverApp.get_Admob_openapp
import java.util.*

class AppOpenManager(private var myApplication: RecoverApp?) : LifecycleObserver,
    Application.ActivityLifecycleCallbacks {


    var appOpenAd: AppOpenAd? = null
    var loadTime: Long = 0
    var isAdShow = false
    private var sharedPreferences: SharedPreferences? = null
    var currentActivity: Activity? = null
    private var isShowingAd = false

    init {
        this.myApplication?.registerActivityLifecycleCallbacks(this)
        ProcessLifecycleOwner.get().lifecycle.addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStart() {
        sharedPreferences = currentActivity?.getSharedPreferences("rating", Context.MODE_PRIVATE)
        val s = currentActivity.toString() + ""
        if (!isAdShow) {
            showAdIfAvailable(false)
        }
    }

    fun fetchAd() {
        try {
            if (isAdAvailable()) {
                return
            }
            val loadCallback: AppOpenAdLoadCallback = object : AppOpenAdLoadCallback() {
                override fun onAdLoaded(appOpenAd: AppOpenAd) {
                    super.onAdLoaded(appOpenAd)
                    this@AppOpenManager.appOpenAd = appOpenAd
                    this@AppOpenManager.loadTime = Date().time
                }

                override fun onAdFailedToLoad(loadAdError: LoadAdError) {
                    super.onAdFailedToLoad(loadAdError)
                }
            }
            val request = getAdRequest()
            AppOpenAd.load(
                myApplication,
                Objects.requireNonNull(get_Admob_openapp()),
                request,
                AppOpenAd.APP_OPEN_AD_ORIENTATION_PORTRAIT,
                loadCallback
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun getAdRequest(): AdRequest {
        return AdRequest.Builder().build()
    }

    private fun isAdAvailable(): Boolean {
        return appOpenAd != null
    }

    override fun onActivityCreated(activity: Activity, bundle: Bundle?) {}

    override fun onActivityStarted(activity: Activity) {
        currentActivity = activity
    }

    override fun onActivityResumed(activity: Activity) {
        currentActivity = activity
    }

    override fun onActivityPaused(activity: Activity) {}

    override fun onActivityStopped(activity: Activity) {}

    override fun onActivitySaveInstanceState(activity: Activity, bundle: Bundle) {}

    override fun onActivityDestroyed(activity: Activity) {
        currentActivity = null
    }

    fun showAdIfAvailable(activity: Boolean) {
        if (!isShowingAd && isAdAvailable()) {
            val fullScreenContentCallback: FullScreenContentCallback =
                object : FullScreenContentCallback() {
                    override fun onAdDismissedFullScreenContent() {
                        try {
                            this@AppOpenManager.appOpenAd = null
                            isShowingAd = false
                            fetchAd()
                        } catch (exception: Exception) {
                            exception.printStackTrace()
                        }
                        if (activity) {
                            if (Utils.hasPermissions(currentActivity, *Constant.PERMISSION)) {
                                if (!currentActivity?.checkManageStoragePermission()!!)
                                    currentActivity?.startActivity<StartActivity> { }
                                else
                                    currentActivity?.startActivity<HomeActivity> { }
                            } else
                                currentActivity?.startActivity<StartActivity> { }
                            currentActivity?.finish()
                        }
                    }

                    override fun onAdFailedToShowFullScreenContent(adError: AdError) {
                        if (activity) {
                            if (Utils.hasPermissions(currentActivity, *Constant.PERMISSION)) {
                                if (!currentActivity?.checkManageStoragePermission()!!)
                                    currentActivity?.startActivity<StartActivity> { }
                                else
                                    currentActivity?.startActivity<HomeActivity> { }
                            } else
                                currentActivity?.startActivity<StartActivity> { }
                            currentActivity?.finish()
                        }
                    }

                    override fun onAdShowedFullScreenContent() {
                        isShowingAd = true
                    }
                }
            appOpenAd?.fullScreenContentCallback = fullScreenContentCallback
            appOpenAd?.show(currentActivity)
        } else {
            fetchAd()
            if (activity) {

                if (Utils.hasPermissions(currentActivity, *Constant.PERMISSION)) {
                    if (!currentActivity?.checkManageStoragePermission()!!)
                        currentActivity?.startActivity<StartActivity> { }
                    else
                        currentActivity?.startActivity<HomeActivity> { }
                } else
                    currentActivity?.startActivity<StartActivity> { }

                currentActivity?.finish()
            }
        }
    }


}