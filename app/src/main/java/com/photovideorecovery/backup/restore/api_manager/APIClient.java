package com.photovideorecovery.backup.restore.api_manager;


import com.photovideorecovery.backup.restore.BuildConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {
    private static Retrofit retrofit;

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(API.Feedback_API)
                    .client(new OkHttpClient.Builder()
                            .addInterceptor(chain -> chain.proceed(chain.request().newBuilder()
                                    .addHeader("Authorization", BuildConfig.APPLICATION_ID)
                                    .addHeader("content-type", "application/json").build()))
                            .connectTimeout(100, TimeUnit.SECONDS)
                            .readTimeout(100, TimeUnit.SECONDS)
                            .build()).addConverterFactory(GsonConverterFactory.create()).build();
        }
        return retrofit;
    }
}
