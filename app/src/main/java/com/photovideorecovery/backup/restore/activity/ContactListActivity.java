package com.photovideorecovery.backup.restore.activity;

import android.annotation.SuppressLint;
import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.photovideorecovery.backup.restore.R;
import com.photovideorecovery.backup.restore.adapter.ContactsAdapter;
import com.photovideorecovery.backup.restore.databinding.ActivityContactBinding;
import com.photovideorecovery.backup.restore.model.Contact;

import java.util.ArrayList;

public class ContactListActivity extends AppCompatActivity {

    private final ArrayList<Contact> allContactList = new ArrayList<>();
    private final ArrayList<Contact> deletedContactList = new ArrayList<>();

    private final ContactsAdapter contactsAdapter = new ContactsAdapter();
    private ContactsAdapter deletedContactAdapter = new ContactsAdapter();
    private ContactPagerAdapter contactPagerAdapter;
    private ActivityContactBinding binding;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        binding = ActivityContactBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        findView();
        new getAllContactsBackTask().execute();
    }

    private void findView() {
        ((Toolbar) findViewById(R.id.toolbar)).setNavigationOnClickListener(v -> onBackPressed());

        binding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    binding.btnAllContact.setBackgroundColor(ContextCompat.getColor(ContactListActivity.this, R.color.red_300));
                    binding.btnDeleted.setBackgroundColor(ContextCompat.getColor(ContactListActivity.this, R.color.red_400));
                } else {
                    binding.btnAllContact.setBackgroundColor(ContextCompat.getColor(ContactListActivity.this,R.color.red_400));
                    binding.btnDeleted.setBackgroundColor(ContextCompat.getColor(ContactListActivity.this,R.color.red_300));
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {}
        });
    }

    @SuppressLint("NonConstantResourceId")
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnAllContact: {
                binding.viewPager.setCurrentItem(0);
                break;
            }
            case R.id.btnDeleted: {
                binding.viewPager.setCurrentItem(1);
                break;
            }
            case R.id.ivMenu: {
                showMenu(view);
                break;
            }
        }
    }

    @SuppressLint("NonConstantResourceId")
    private void showMenu(View view) {

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.layout_menu, null);

        int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.45);
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, true);
        popupWindow.setElevation(100f);
        popupWindow.showAsDropDown(view, 0, 0);
        popupView.findViewById(R.id.btnRefresh).setOnClickListener(view1 -> {
            new getAllContactsBackTask().execute();
            popupWindow.dismiss();
        });
        popupView.findViewById(R.id.btnRestoreAll).setOnClickListener(view1 -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(ContactListActivity.this, R.style.AlertDialogTheme);
            builder.setMessage("Do you Want Restore All Contacts ??");
            builder.setPositiveButton("Yes", (dialog, which) -> {
                new restoreAllContactsBackTask().execute();
                dialog.dismiss();
            });
            builder.setNegativeButton("No", (dialog, which) -> dialog.dismiss());
            AlertDialog dialog = builder.create();
            dialog.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(ContactListActivity.this, R.drawable.back_white));
            dialog.show();
            popupWindow.dismiss();
        });

    }

    public void showToast(ContactListActivity contactListActivity, String str) {
        Toast makeText = Toast.makeText(contactListActivity, str, Toast.LENGTH_SHORT);
        makeText.setGravity(Gravity.CENTER, 0, 0);
        makeText.show();
    }

    @Override
    public void onBackPressed() {
        allContactList.clear();
        deletedContactList.clear();
        if (deletedContactAdapter != null)
            deletedContactAdapter.setListener(null);

        if (deletedContactAdapter != null)
            deletedContactAdapter = null;
        if (contactPagerAdapter != null)
            contactPagerAdapter = null;

        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
        finish();

    }

    public class ContactPagerAdapter extends PagerAdapter {

        public final String[] titles = {"All", "Deleted"};
        public Context context;

        public ContactPagerAdapter(Context context) {
            this.context = context;
        }

        @Override
        public void destroyItem(ViewGroup viewGroup, int i, @NonNull Object obj) {
            viewGroup.removeView((View) obj);
        }

        @Override
        public int getCount() {
            return this.titles.length;
        }

        @Override
        public CharSequence getPageTitle(int i) {
            return this.titles[i];
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup viewGroup, int i) {

            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View inflate = layoutInflater.inflate(R.layout.contact_viewpager, viewGroup, false);
            final RecyclerView rvContact = inflate.findViewById(R.id.rvContact);
            final TextView tvNoContactFound = inflate.findViewById(R.id.tvNoContact);

            if (i == 0) {
                if (allContactList.size() > 0) {
                    tvNoContactFound.setVisibility(View.GONE);
                    rvContact.setVisibility(View.VISIBLE);
                } else {
                    tvNoContactFound.setVisibility(View.VISIBLE);
                    rvContact.setVisibility(View.GONE);
                    tvNoContactFound.setText(getResources().getString(R.string.no_deleted_contacts));
                }

                contactsAdapter.setContactList(allContactList);
                rvContact.setAdapter(contactsAdapter);
            } else {
                deletedContactAdapter.setContactList(deletedContactList);
                rvContact.setAdapter(deletedContactAdapter);
                deletedContactAdapter.setListener(contact -> {
                    if (binding.viewPager.getCurrentItem() == 1) {
                        new ContentValues().put("deleted", "0");
                        AlertDialog.Builder builder = new AlertDialog.Builder(ContactListActivity.this, R.style.AlertDialogTheme);
                        builder.setMessage("Do you want to restore all contacts?");
                        builder.setPositiveButton("Yes", (dialogInterface, i1) -> {

                            try {
                                ArrayList<ContentProviderOperation> arrayList = new ArrayList<>();
                                arrayList.add(ContentProviderOperation
                                        .newUpdate(ContactsContract.RawContacts.CONTENT_URI.buildUpon().appendQueryParameter(ContactsContract.CALLER_IS_SYNCADAPTER, "true").build())
                                        .withSelection("_id=?", new String[]{String.valueOf(contact.getContactRawId())}).withValue("deleted", 0).withYieldAllowed(true).build());
                                getContentResolver().applyBatch("com.android.contacts", arrayList);
                                deletedContactList.remove(contact);
                                deletedContactAdapter.setContactList(deletedContactList);
                                if (deletedContactList.size() > 0) {
                                    tvNoContactFound.setVisibility(View.GONE);
                                    rvContact.setVisibility(View.VISIBLE);
                                } else {
                                    tvNoContactFound.setVisibility(View.VISIBLE);
                                    rvContact.setVisibility(View.GONE);
                                    tvNoContactFound.setText(getResources().getString(R.string.no_deleted_contacts));
                                }

                                new getAllContactsBackTask().execute();

                                showToast(ContactListActivity.this, getResources().getString(R.string.contact_restore_success));
                                dialogInterface.dismiss();
                            } catch (RemoteException | OperationApplicationException e) {
                                e.printStackTrace();
                                showToast(ContactListActivity.this, getResources().getString(R.string.contact_not_restore));

                                dialogInterface.dismiss();
                            }
                        });
                        builder.setNegativeButton("No", (dialog, which) -> dialog.dismiss());
                        AlertDialog dialog = builder.create();
                        dialog.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(ContactListActivity.this, R.drawable.back_white));
                        dialog.show();
                    }
                });
                if (deletedContactList.size() > 0) {
                    tvNoContactFound.setVisibility(View.GONE);
                    rvContact.setVisibility(View.VISIBLE);
                } else {
                    tvNoContactFound.setVisibility(View.VISIBLE);
                    rvContact.setVisibility(View.GONE);
                    tvNoContactFound.setText(getResources().getString(R.string.no_deleted_contacts));
                }

                viewGroup.addView(inflate);
                return inflate;
            }

            viewGroup.addView(inflate);
            return inflate;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object obj) {
            return view == obj;
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class getAllContactsBackTask extends AsyncTask<String, String, String> {

        public String getContact() {

            Contact contact;
            try {
                allContactList.clear();
                deletedContactList.clear();
                Cursor query = getContentResolver().query(ContactsContract.RawContacts.CONTENT_URI, null, "sort_key" + " IS NOT NULL", null, "sort_key" + " COLLATE LOCALIZED ASC");
                if (query != null && query.getCount() > 0 && query.moveToFirst()) {
                    while (!query.isAfterLast()) {
                        boolean isDeleted = false;

                        String contactRawId = String.valueOf(query.getInt(query.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID)));
                        String contactId = "";
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                            contactId = String.valueOf(query.getInt(query.getColumnIndexOrThrow(ContactsContract.PhoneLookup.CONTACT_ID)));
                        }
                        if (query.getInt(query.getColumnIndexOrThrow(ContactsContract.RawContacts.DELETED)) == 1) {
                            isDeleted = true;
                        }
                        String contactName = query.getString(query.getColumnIndexOrThrow("sort_key"));
                        if (isDeleted) {
                            contact = new Contact(contactRawId, contactId, contactName, "yes");
                            deletedContactList.add(contact);
                        } else {
                            contact = new Contact(contactRawId, contactId, contactName, "no");
                            allContactList.add(contact);
                        }

                        query.moveToNext();
                    }
                }
                if (query != null) {
                    query.close();
                }
            } catch (Exception exception) {
                exception.printStackTrace();
            }
            return "";
        }

        @Override
        public String doInBackground(String[] strArr) {
            return getContact();
        }

        @Override
        public void onPostExecute(String str) {
            super.onPostExecute(str);

            new Handler().postDelayed(() -> {
                if (binding.progressBar.getVisibility() == View.VISIBLE) {
                    binding.progressBar.setVisibility(View.GONE);
                }
                runOnUiThread(() -> {
                    contactPagerAdapter = new ContactPagerAdapter(ContactListActivity.this);
                    binding.viewPager.setAdapter(contactPagerAdapter);

                    binding.viewPager.setCurrentItem(0);
                    findViewById(R.id.btnAllContact).setBackgroundColor(ContextCompat.getColor(ContactListActivity.this, R.color.red_300));
                    findViewById(R.id.btnDeleted).setBackgroundColor(ContextCompat.getColor(ContactListActivity.this, R.color.red_400));
                    cancel(true);
                });
            }, 1000);

        }

        public void onPreExecute() {
            super.onPreExecute();
            binding.progressBar.setVisibility(View.VISIBLE);
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class restoreAllContactsBackTask extends AsyncTask<String, String, String> {

        public String msg = "";

        public String restoreContact() {

            if (deletedContactList.size() > 0) {
                for (int i = 0; i < deletedContactList.size(); i++) {
                    Contact contact = deletedContactList.get(i);
                    new ContentValues().put("deleted", "0");
                    try {
                        ArrayList<ContentProviderOperation> arrayList = new ArrayList<>();
                        arrayList.add(ContentProviderOperation.newUpdate(ContactsContract.RawContacts.CONTENT_URI.buildUpon().appendQueryParameter(ContactsContract.CALLER_IS_SYNCADAPTER, "true").build()).withSelection("_id=?", new String[]{String.valueOf(contact.getContactRawId())}).withValue(
                                "deleted", 0).withYieldAllowed(true).build());
                        getContentResolver().applyBatch("com.android.contacts", arrayList);
                        this.msg = getResources().getString(R.string.all_contact_restore_success);
                    } catch (RemoteException e) {
                        try {
                            e.printStackTrace();
                            this.msg = getResources().getString(R.string.all_contact_not_restore);
                        } catch (Exception e2) {
                            this.msg = getResources().getString(R.string.all_contact_not_restore);

                        }
                    } catch (OperationApplicationException e3) {
                        e3.printStackTrace();
                        this.msg = getResources().getString(R.string.all_contact_not_restore);


                    }
                }
            }
            return this.msg;
        }


        @Override
        public String doInBackground(String[] strArr) {
            return restoreContact();
        }

        @Override
        public void onPostExecute(String str) {
            final String str2 = str;
            super.onPostExecute(str2);
            if (binding.progressBar.getVisibility() == View.VISIBLE) {
                binding.progressBar.setVisibility(View.GONE);
            }
            runOnUiThread(() -> {
                Resources resources = getResources();
                int i = R.string.all_contact_restore_success;
                if (str2.equalsIgnoreCase(resources.getString(R.string.all_contact_restore_success))) {
                    deletedContactList.clear();
                    deletedContactAdapter.setContactList(deletedContactList);
                    contactPagerAdapter = new ContactPagerAdapter(ContactListActivity.this);
                    binding.viewPager.setAdapter(contactPagerAdapter);


                    new getAllContactsBackTask().execute();
                } else {
                    String str21 = "";
                    Resources resources2 = getResources();
                    i = R.string.all_contact_not_restore;
                    if (!str21.equalsIgnoreCase(resources2.getString(R.string.all_contact_not_restore))) {
                        return;
                    }
                }
                showToast(ContactListActivity.this, getResources().getString(i));
                cancel(true);
            });
        }

        public void onPreExecute() {
            super.onPreExecute();
            binding.progressBar.setVisibility(View.VISIBLE);
        }
    }


}
