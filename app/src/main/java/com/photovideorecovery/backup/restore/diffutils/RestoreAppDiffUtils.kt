package com.photovideorecovery.backup.restore.diffutils

import androidx.recyclerview.widget.DiffUtil
import com.photovideorecovery.backup.restore.model.RestoreModel

class RestoreAppDiffUtils(
    private val oldList: ArrayList<RestoreModel>,
    private val newList: ArrayList<RestoreModel>
) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] === newList[newItemPosition]
    }
}