package com.photovideorecovery.backup.restore.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.signature.ObjectKey
import com.photovideorecovery.backup.restore.R
import com.photovideorecovery.backup.restore.databinding.ItemImageBinding
import com.photovideorecovery.backup.restore.extension.hide
import com.photovideorecovery.backup.restore.extension.show
import com.photovideorecovery.backup.restore.model.ImageObject


class RecoverFileAdapter : RecyclerView.Adapter<RecoverFileAdapter.Holder>() {

    val selectedList = arrayListOf<ImageObject>()

    var list = listOf<ImageObject>()
        set(value) {
            field = value
            notifyItemRangeChanged(0, field.size)
        }

    inner class Holder(val binding: ItemImageBinding) : RecyclerView.ViewHolder(binding.root) {
        val context: Context = itemView.context
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        Holder(ItemImageBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.binding.tvFolderCount.hide()
        if (list[position].isCheck()) {
            holder.binding.ivSelect.show()
            holder.binding.ivSelect.setImageDrawable(
                ContextCompat.getDrawable(
                    holder.context,
                    R.drawable.ic_select
                )
            )
            holder.binding.image.alpha = 0.65f
        } else {
            holder.binding.ivSelect.hide()
            holder.binding.ivSelect.setImageDrawable(null)
            holder.binding.image.alpha = 1f
        }

        holder.binding.image.setOnClickListener { view: View ->
            if (list[position].isCheck()) {
                holder.binding.ivSelect.hide()
                list[position].setCheck(false)
                holder.binding.ivSelect.setImageDrawable(null)
                holder.binding.image.alpha = 1.0f
                selectedList.remove(list[position])
            } else {
                holder.binding.ivSelect.show()
                list[position].setCheck(true)
                holder.binding.ivSelect.setImageDrawable(
                    ContextCompat.getDrawable(
                        view.context,
                        R.drawable.ic_select
                    )
                )
                holder.binding.image.alpha = 0.65f
                selectedList.add(list[position])
            }
        }


        Glide.with(holder.context)
            .asBitmap()
            .load(list[position].path)
            .placeholder(R.drawable.ic_loading_image)
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .skipMemoryCache(true)
            .signature(ObjectKey(System.currentTimeMillis()))
            .fitCenter()
            .into(holder.binding.image)
    }

    override fun getItemCount() = list.size
}