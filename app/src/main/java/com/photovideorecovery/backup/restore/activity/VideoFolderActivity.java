package com.photovideorecovery.backup.restore.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.Group;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.photovideorecovery.backup.restore.R;
import com.photovideorecovery.backup.restore.common.Utils;
import com.photovideorecovery.backup.restore.model.ImageObject;
import com.photovideorecovery.backup.restore.utils.Constant;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

@SuppressLint("NotifyDataSetChanged")
public class VideoFolderActivity extends AppCompatActivity {

    private ArrayList<String> imagePathList = new ArrayList<>();
    private ArrayList<ImageObject> imageObjectList = new ArrayList<>();
    private ArrayList<String> savedSelectedPathList = new ArrayList<>();
    private ArrayList<String> selectedPathList = new ArrayList<>();
    private ArrayList<Integer> selectedPositionList = new ArrayList<>();

    private VideoFolderAdapter videoFolderAdapter;

    private Group groupBtn;
    private ProgressBar progressBar;

    private String folderName;
    private boolean isClick = false;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_video_folder);

        findView();
        initViews();
    }

    private void findView() {
        ((Toolbar) findViewById(R.id.toolbar)).setNavigationIcon(R.drawable.ic_back);
        ((Toolbar) findViewById(R.id.toolbar)).setNavigationOnClickListener(v -> onBackPressed());
        ((TextView) findViewById(R.id.tvTitle)).setText(R.string.str_deleted_video_recovery);

        groupBtn = findViewById(R.id.groupBtn);
        progressBar = findViewById(R.id.progressBar);
        findViewById(R.id.ivMenu).setVisibility(View.GONE);
    }

    private void initViews() {

        int intExtra = getIntent().getIntExtra("position", 0);

        this.imagePathList = VideoListActivity.imagePathList.get(intExtra);
        for (String s : imagePathList) {
            this.imageObjectList.add(new ImageObject(s, false));
        }
        this.folderName = VideoListActivity.folderList.get(intExtra);
        this.videoFolderAdapter = new VideoFolderAdapter(this, this.imageObjectList);

        if (this.imageObjectList.size() == 0) {
            findViewById(R.id.progressBar).setVisibility(View.GONE);
            Toast.makeText(this, R.string.no_image, Toast.LENGTH_SHORT).show();
        }
        ((RecyclerView) findViewById(R.id.recyclerview)).setAdapter(this.videoFolderAdapter);

    }

    @SuppressLint("NonConstantResourceId")
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSelectAll: {
                selectAllFiles();
                groupBtn.setVisibility(View.VISIBLE);
                break;
            }
            case R.id.btnUnSelectAll: {
                unSelectAllFiles();
                groupBtn.setVisibility(View.GONE);
                break;
            }
            case R.id.btnDeleted: {
                if (selectedPathList.size() == 0) {
                    Toast.makeText(this, "Please select at least one file", Toast.LENGTH_SHORT).show();
                    return;
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(R.string.str_msg_delet_file);
                builder.setPositiveButton("Yes", (dialog, which) -> deleteSelectedFile());
                builder.setNegativeButton("No", (dialog, which) -> dialog.dismiss());
                AlertDialog dialog = builder.create();
                dialog.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.back_white));
                dialog.show();
                break;
            }
            case R.id.btnRestore: {
                if (selectedPathList.size() == 0) {
                    Toast.makeText(this, "Please select at least one file", Toast.LENGTH_SHORT).show();
                    return;
                }
                new RestoreVideoTask().execute();
                break;
            }
        }

    }

    private void deleteSelectedFile() {
        Collections.sort(selectedPositionList);
        Iterator<String> it = selectedPathList.iterator();
        int i = 0;
        while (it.hasNext()) {
            String next = it.next();
            File file = new File(next);
            if (!file.delete()) {
                Toast.makeText(this, getString(R.string.str_file_can_not_deleted), Toast.LENGTH_SHORT).show();
            }
            imagePathList.remove(next);
            imageObjectList.remove(selectedPositionList.get(i) - i);
            savedSelectedPathList.add(next);
            i++;
        }

        isClick = true;
        videoFolderAdapter.notifyDataSetChanged();
        String msg;
        if (i == 1)
            msg = i + " " + "video deleted";
        else
            msg = i + " " + "videos deleted";

        Toast.makeText(VideoFolderActivity.this, msg, Toast.LENGTH_SHORT).show();
        selectedPositionList.clear();
        selectedPathList.clear();
        groupBtn.setVisibility(View.GONE);
    }

    private void unSelectAllFiles() {
        ((AppCompatImageView) findViewById(R.id.ivMenu)).setColorFilter(
                ContextCompat.getColor(this, R.color.black),
                android.graphics.PorterDuff.Mode.SRC_IN);

        selectedPathList.clear();
        selectedPositionList.clear();
        for (int i = 0; i < imageObjectList.size(); i++) {
            videoFolderAdapter.imageObjectList.get(i).setCheck(false);
        }
        videoFolderAdapter.notifyDataSetChanged();
        groupBtn.setVisibility(View.GONE);
    }

    private void selectAllFiles() {
        selectedPathList.clear();
        selectedPositionList.clear();
        for (int i = 0; i < imageObjectList.size(); i++) {
            videoFolderAdapter.imageObjectList.get(i).setCheck(true);
            selectedPathList.add(imageObjectList.get(i).getPath());
            selectedPositionList.add(i);
        }
        videoFolderAdapter.notifyDataSetChanged();
        groupBtn.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        Intent intent = new Intent("Deleted Paths");
        intent.putExtra("Deleted List", this.savedSelectedPathList);
        intent.putExtra("folder_name", this.folderName);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        finish();
        return true;
    }


    @Override
    public void onBackPressed() {
        if (!this.isClick) {
            this.selectedPathList.clear();
        }
        if (imagePathList != null)
            imagePathList = null;
        if (imageObjectList != null)
            imageObjectList = null;
        if (savedSelectedPathList != null)
            savedSelectedPathList = null;
        if (selectedPathList != null)
            selectedPathList = null;
        if (selectedPositionList != null)
            selectedPositionList = null;
        if (videoFolderAdapter != null)
            videoFolderAdapter = null;

        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
        finish();

    }

    @SuppressLint("StaticFieldLeak")
    public class RestoreVideoTask extends AsyncTask<Void, Void, Void> {

        public Void restoreVideoFile() {
            Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            try {
                for (String selectedPath : selectedPathList) {
                    String[] split = selectedPath.split("/");
                    File currentFile = new File(selectedPath);
                    String str = "";
                    if (!split[split.length - 1].contains(".")) {
                        str = ".mp4";
                    }
                    String BACKUP_VIDEO_FOLDER = Constant.BASE_PATH + getString(R.string.app_name) + File.separator + getString(R.string.video) + File.separator;

                    File restoreFile = new File(BACKUP_VIDEO_FOLDER + split[split.length - 1] + str);
                    try {
                        Utils.copyDirectory(VideoFolderActivity.this, currentFile, restoreFile);
                        intent.setData(Uri.fromFile(restoreFile));
                        sendBroadcast(intent);
                        publishProgress();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return null;
            } catch (Exception unused) {
                return null;
            }
        }

        @Override
        public Void doInBackground(Void[] voidArr) {
            return restoreVideoFile();
        }

        @Override
        @SuppressLint("NotifyDataSetChanged")
        public void onPostExecute(Void r3) {
            super.onPostExecute(r3);
            Toast.makeText(VideoFolderActivity.this, "Video successfully recovered.", Toast.LENGTH_LONG).show();
            selectedPathList.clear();
            groupBtn.setVisibility(View.GONE);
            for (int i = 0; i < imageObjectList.size(); i++) {
                videoFolderAdapter.imageObjectList.get(i).setCheck(false);
            }
            videoFolderAdapter.notifyDataSetChanged();
            progressBar.setVisibility(View.GONE);

        }

        public void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    public class VideoFolderAdapter extends RecyclerView.Adapter<VideoFolderAdapter.ViewHolder> {
        public final TypedValue typedValue = new TypedValue();
        public int background;
        public List<ImageObject> imageObjectList;

        public VideoFolderAdapter(Context context, List<ImageObject> list) {
            context.getTheme().resolveAttribute(R.attr.selectableItemBackground, this.typedValue, true);
            this.background = typedValue.resourceId;
            this.imageObjectList = list;

        }

        @Override
        public int getItemCount() {
            return this.imageObjectList.size();
        }

        public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int position) {

            if (imageObjectList.get(position).isCheck()) {
                viewHolder.ivSelect.setImageDrawable(ContextCompat.getDrawable(viewHolder.itemView.getContext(), R.drawable.ic_select_video));
                viewHolder.ivMain.setAlpha(0.7f);
            } else {
                viewHolder.ivSelect.setImageDrawable(null);
                viewHolder.ivMain.setAlpha(1.0f);
            }

            viewHolder.ivMain.setOnClickListener(view -> {
                if (imageObjectList.get(position).isCheck()) {
                    imageObjectList.get(position).setCheck(false);
                    viewHolder.ivSelect.setImageDrawable(null);
                    viewHolder.ivMain.setAlpha(1.0f);
                    selectedPathList.remove(imagePathList.get(position));
                    selectedPositionList.remove(Integer.valueOf(position));
                } else {
                    imageObjectList.get(position).setCheck(true);
                    viewHolder.ivSelect.setImageDrawable(ContextCompat.getDrawable(viewHolder.itemView.getContext(), R.drawable.ic_select_video));
                    viewHolder.ivMain.setAlpha(0.7f);
                    selectedPathList.add(imagePathList.get(position));
                    selectedPositionList.add(position);
                }
                if (selectedPositionList.size() == 0)
                    groupBtn.setVisibility(View.GONE);
                else
                    groupBtn.setVisibility(View.VISIBLE);
                isClick = false;
            });

            Glide.with(viewHolder.ivMain.getContext())
                    .asBitmap()
                    .load(this.imageObjectList.get(position).getPath())
                    .placeholder(R.drawable.ic_loading_image)
                    .fitCenter()
                    .into(viewHolder.ivMain);
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_image, viewGroup, false);
            inflate.setBackgroundResource(this.background);
            return new ViewHolder(inflate);
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public final ImageView ivSelect;
            public final ImageView ivMain;

            public ViewHolder(View view) {
                super(view);
                this.ivMain = view.findViewById(R.id.image);

                ivSelect = view.findViewById(R.id.ivSelect);
                ivSelect.setVisibility(View.VISIBLE);
                view.findViewById(R.id.tvFolderCount).setVisibility(View.INVISIBLE);
            }

        }
    }


}
