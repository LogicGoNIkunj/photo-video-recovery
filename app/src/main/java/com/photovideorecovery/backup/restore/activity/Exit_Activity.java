package com.photovideorecovery.backup.restore.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.ads.nativead.NativeAdView;
import com.photovideorecovery.backup.restore.BuildConfig;
import com.photovideorecovery.backup.restore.R;
import com.photovideorecovery.backup.restore.fragment.FeedbackDialogFragment;
import com.photovideorecovery.backup.restore.utils.RecoverApp;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;


public class Exit_Activity extends AppCompatActivity {
    FrameLayout frmlay_exit;
    NativeAd nativeAds_exit;
    RatingBar ratingbar;
    SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exit);
        getWindow().setBackgroundDrawable(null);
        sharedPreferences = getSharedPreferences("rating", Context.MODE_PRIVATE);
        init();
        RefreshAd();
        click();
    }

    private void init() {
        frmlay_exit = findViewById(R.id.Admob_Native_Frame);
        ratingbar = findViewById(R.id.ratestar);
        findViewById(R.id.exit_card).setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (sharedPreferences.getBoolean("ratePref", true)) {
            findViewById(R.id.ll_rate).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.ll_rate).setVisibility(View.GONE);
        }
    }

    @SuppressLint("SetTextI18n")
    public void click() {

        findViewById(R.id.back_btn).setOnClickListener(v -> finish());
        findViewById(R.id.notnow).setOnClickListener(v -> finish());

        findViewById(R.id.exit).setOnClickListener(view -> {
            RecoverApp.mAppOpenManager.setAdShow(true);
            finishAffinity();
        });
        ratingbar.setOnRatingBarChangeListener((ratingBar, v, b) -> {
            if (ratingBar.getRating() <= 3) {
                FeedbackDialogFragment dialogFragment = new FeedbackDialogFragment();
                dialogFragment.show(getSupportFragmentManager(), "FeedbackDialogFragment");
            } else {
                startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID)));
                SharedPreferences.Editor editor1;
                editor1 = sharedPreferences.edit();
                editor1.putBoolean("ratePref", false);
                editor1.apply();
                findViewById(R.id.ll_rate).setVisibility(View.GONE);
            }
        });
    }


    private void RefreshAd() {

        AdLoader.Builder builder = new AdLoader.Builder(this, RecoverApp.get_Admob_native_Id())
                .forNativeAd(nativeAd -> {

                    nativeAds_exit = nativeAd;

                    @SuppressLint("InflateParams") NativeAdView adView = (NativeAdView) getLayoutInflater()
                            .inflate(R.layout.layout_unified_native_ad, null);
                    populateUnifiedNativeAdView(nativeAd, adView);
                    frmlay_exit.removeAllViews();
                    frmlay_exit.addView(adView);
                    findViewById(R.id.space).setVisibility(View.GONE);
                });
        AdLoader adLoader = builder.withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(@NonNull @NotNull LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);

            }
        }).build();

        adLoader.loadAd(new AdRequest.Builder().build());


    }

    public void populateUnifiedNativeAdView(NativeAd nativeAd, NativeAdView adView) {
        com.google.android.gms.ads.nativead.MediaView mediaView = adView.findViewById(R.id.ad_media);
        adView.setMediaView(mediaView);
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_app_icon));
        ((TextView) Objects.requireNonNull(adView.getHeadlineView())).setText(nativeAd.getHeadline());
        if (nativeAd.getBody() == null) {
            Objects.requireNonNull(adView.getBodyView()).setVisibility(View.INVISIBLE);
        } else {
            Objects.requireNonNull(adView.getBodyView()).setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        }
        if (nativeAd.getCallToAction() == null) {
            Objects.requireNonNull(adView.getCallToActionView()).setVisibility(View.INVISIBLE);
        } else {
            Objects.requireNonNull(adView.getCallToActionView()).setVisibility(View.VISIBLE);
            ((TextView) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
        }
        if (nativeAd.getIcon() == null) {
            Objects.requireNonNull(adView.getIconView()).setVisibility(View.GONE);
        } else {
            ((ImageView) Objects.requireNonNull(adView.getIconView())).setImageDrawable(
                    nativeAd.getIcon().getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }
        adView.setNativeAd(nativeAd);
    }




    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (nativeAds_exit != null) {
            nativeAds_exit.destroy();
        }
    }
}
