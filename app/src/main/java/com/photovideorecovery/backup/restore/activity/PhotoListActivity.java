package com.photovideorecovery.backup.restore.activity;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.ads.nativead.NativeAdView;
import com.photovideorecovery.backup.restore.R;
import com.photovideorecovery.backup.restore.common.PhotoScanner;
import com.photovideorecovery.backup.restore.utils.RecoverApp;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Objects;

public class PhotoListActivity extends AppCompatActivity {

    public static ArrayList<String> folderList = new ArrayList<>();
    public static ArrayList<ArrayList<String>> imagePathList = new ArrayList<>();
    public ArrayList<String> deletedImageList = new ArrayList<>();
    public ArrayList<String> thumbnailList = new ArrayList<>();
    public HashMap<String, ArrayList<String>> folderOfImage = new HashMap<>();
    public ImageAdapter imageAdapter;
    public String selectedFolder;

    public BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            deletedImageList = intent.getStringArrayListExtra("Deleted List");
            selectedFolder = intent.getStringExtra("folder_name");
        }
    };

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_list);
        admobBigNative(findViewById(R.id.flNativeAdd));
        initView();
    }

    @SuppressLint("MissingPermission")
    public void admobBigNative(FrameLayout admob_native_ll) {
        AdLoader.Builder builder = new AdLoader.Builder(this, RecoverApp.get_Admob_native_Id()).forNativeAd(nativeAd -> {
            @SuppressLint("InflateParams") NativeAdView adView = (NativeAdView) getLayoutInflater().inflate(R.layout.native_ads_small, null);
            populateUnifiedNativeAdView(nativeAd, adView);

            admob_native_ll.removeAllViews();
            admob_native_ll.addView(adView);
        });
        AdLoader adLoader = builder.withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);

            }
        }).build();
        adLoader.loadAd(new AdRequest.Builder().build());
    }

    public void populateUnifiedNativeAdView(NativeAd nativeAd, NativeAdView adView) {
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_app_icon));
        ((TextView) Objects.requireNonNull(adView.getHeadlineView())).setText(nativeAd.getHeadline());
        if (nativeAd.getBody() == null) {
            Objects.requireNonNull(adView.getBodyView()).setVisibility(View.INVISIBLE);
        } else {
            Objects.requireNonNull(adView.getBodyView()).setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        }
        if (nativeAd.getCallToAction() == null) {
            Objects.requireNonNull(adView.getCallToActionView()).setVisibility(View.INVISIBLE);
        } else {
            Objects.requireNonNull(adView.getCallToActionView()).setVisibility(View.VISIBLE);
            ((TextView) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
        }
        if (nativeAd.getIcon() == null) {
            Objects.requireNonNull(adView.getIconView()).setVisibility(View.GONE);
        } else {
            ((ImageView) Objects.requireNonNull(adView.getIconView())).setImageDrawable(nativeAd.getIcon().getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }
        adView.setNativeAd(nativeAd);
    }

    private void initView() {
        ((Toolbar) findViewById(R.id.toolbar)).setNavigationIcon(R.drawable.ic_back);
        ((Toolbar) findViewById(R.id.toolbar)).setNavigationOnClickListener(v -> onBackPressed());
        ((TextView) findViewById(R.id.tvTitle)).setText(R.string.deleted_image_recovery);
        findViewById(R.id.ivMenu).setVisibility(View.GONE);

    }

    @Override
    public void onBackPressed() {

        if (imageAdapter != null)
            imageAdapter = null;
        if (folderList != null)
            folderList = null;
        if (imagePathList != null)
            imagePathList = null;
        if (deletedImageList != null)
            deletedImageList = null;
        if (thumbnailList != null)
            thumbnailList = null;
        if (folderOfImage != null)
            folderOfImage = null;

        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
        finish();

    }

    private void setData() {
        try {
            Iterator<String> it = this.deletedImageList.iterator();
            while (it.hasNext()) {
                try {
                    Objects.requireNonNull(this.folderOfImage.get(this.selectedFolder)).remove(it.next());
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }

            if (folderList != null) {
                folderList.clear();
                imagePathList.clear();
                thumbnailList.clear();
            } else {
                folderList = new ArrayList<>();
                imagePathList = new ArrayList<>();
                thumbnailList = new ArrayList<>();
            }

            for (String str : this.folderOfImage.keySet()) {
                if (Objects.requireNonNull(this.folderOfImage.get(str)).size() > 0) {
                    imagePathList.add(this.folderOfImage.get(str));
                }
            }
            Collections.sort(imagePathList, (arrayList, arrayList2) -> arrayList2.size() - arrayList.size());

            for (ArrayList<String> next : imagePathList) {
                try {
                    for (String deleted_image : this.deletedImageList) {
                        next.remove(deleted_image);
                    }
                    if (next.size() > 0) {
                        String str2 = next.get(0);
                        this.thumbnailList.add(str2);
                        String[] split = str2.split("/");
                        folderList.add(str2.substring(str2.length() - split[split.length - 1].length()));
                    }
                } catch (Exception ignored) {
                }
            }

            if (imageAdapter == null) {
                this.imageAdapter = new ImageAdapter(this, this.thumbnailList);
            }
            new Handler().postDelayed(() -> {
                ((RecyclerView) findViewById(R.id.rvImageList)).setAdapter(imageAdapter);
                findViewById(R.id.progressBar).setVisibility(View.GONE);
            }, 1500);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        LocalBroadcastManager.getInstance(this).registerReceiver(this.broadcastReceiver, new IntentFilter("Deleted Paths"));
        this.folderOfImage = PhotoScanner.folderOfImage;

        new Handler().postDelayed(this::setData, 100);

    }

    public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ViewHolder> {
        private final int background;
        private final ArrayList<String> imageList;

        public class ViewHolder extends RecyclerView.ViewHolder {
            public final TextView tvCount;
            public final ImageView ivMain;

            public ViewHolder(View view) {
                super(view);

                ivMain = view.findViewById(R.id.image);
                tvCount = view.findViewById(R.id.tvFolderCount);
            }
        }

        public ImageAdapter(Context context, ArrayList<String> list) {
            TypedValue typedValue = new TypedValue();
            context.getTheme().resolveAttribute(R.attr.selectableItemBackground, typedValue, true);
            this.background = typedValue.resourceId;
            this.imageList = list;
        }

        @Override
        public int getItemCount() {
            return imageList.size();
        }


        public void onBindViewHolder(@NonNull ViewHolder viewHolder, @SuppressLint({"RecyclerView"}) final int i) {

            if (imagePathList.get(i).size() != 0) {

                viewHolder.tvCount.setText(Objects.requireNonNull(new File(imagePathList.get(i).get(0)).getParentFile()).getName());

                Glide.with(viewHolder.ivMain.getContext())
                        .load(this.imageList.get(i))
                        .placeholder(R.drawable.ic_loading_image)
                        .fitCenter()
                        .into(viewHolder.ivMain);

                viewHolder.ivMain.setOnClickListener(view -> {
                    try {
                        Intent intent = new Intent(PhotoListActivity.this, PhotoFolderActivity.class);
                        if (imagePathList.get(i).size() != 0)
                            intent.putExtra("position", i);
                        else
                            intent.putExtra("position", i + 1);

                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                    } catch (Exception unused) {
                        Toast.makeText(PhotoListActivity.this, getString(R.string.str_something_went_wrong), Toast.LENGTH_LONG).show();
                    }
                });
            }

        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_image, viewGroup, false);
            inflate.setBackgroundResource(this.background);
            return new ViewHolder(inflate);
        }
    }


}